var PDFDocument = require('pdfkit');
var fs = require('fs');

blobStream = require('blob-stream');


function downloadFile(res) {

  console.log("downloadFile")

  pdfCreator();

  setTimeout(function () {
    var file = __dirname + '/out.pdf';

    res.download(file);

  }, 100);

}


function pdfCreator() {

  console.log("end");

  doc = new PDFDocument;

  stream = doc.pipe(blobStream())

  doc.pipe(fs.createWriteStream(__dirname + '/out.pdf'));

  doc.fontSize(25)
    .text('Some text with an embedded font!', 100, 100)

  doc.table([
    ["cell11","cell21","cell31"],
    ["cell12","cell22","cell32"],
    ["cell13","cell23","cell33"]
  ],{
    width:20,
    height:40,
    x:30,
    y:40
  });

  doc.end();

  console.log("end");
}


module.exports = {

  downloadFile: downloadFile

};



