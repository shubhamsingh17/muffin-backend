//type of notifications: auction started(auction), invitation into muffin(invite), normal message 
//displaying(normal) 
//



var config = require('../config/environment');
var gcm = require('node-gcm');

var _ = require('lodash');
var ENV = config.env;

var Devices = require('../api/devices/devices.model');
var MuffinForUsers = require('../api/muffins-for-users/muffins-for-users.model');


// var apnConfigCustomer = config.apns_customer;

var notificationTimeConst = Number(12*60*60*1000);
var milliSecondsInHour = Number(60*60*1000);
var milliSecondsInMin = Number(60*1000);
var hoursInDay = 24;

var restrictedTime = {
  start: '22:00:00',
  end: '08:00:00'
}


var sender = new gcm.Sender(config.gcm.key);
//var sender_customer = new gcm.Sender(config.gcm.android_customer);

var sendGCM = function (deviceIds, message) {

  var gcmMessage = new gcm.Message();
  gcmMessage.addData(message);
  sender.send(gcmMessage, deviceIds, function (err, result) {
    if (err)
      console.error(err);
    else
      console.log(result);
  });
};

var notificationId = function (count) {
  var chars = '0123456789'.split('');
  var result = '';
  for (var i = 0; i < count; i++) {
    var x = Math.floor(Math.random() * chars.length);
    result += chars[x];
  }
  return result;
};



function getTotalDelay(type, current_delay) {
  var delay_list = checkRestrictedTimeDuration(current_delay);
  var extraDelay = delay_list.delay;
  switch(type) {
    case 'newResponse':
    case 'digestNotification':
    if (extraDelay > 0) {
      // in the morning
      var random = Math.floor(Math.random() * 60000);
      extraDelay = delay_list.time_in_window + random;
      extraDelay = 0;//send real time
    }
    break;
  }
  return extraDelay + current_delay;
}


function sendPush(user_id, title, message, type, data){

  Devices.find({user_id:{$in:[user_id]}}, function(err, success){
                var devices =[];

                _.forEach(success, function(data, err){

                devices.push(data.device.gcm_id);

             });

          var dataToPush = {title:title, message:message, type:type, data:data};

          console.log(devices);
          sendGCM(devices, dataToPush);

      });

}



function normalPush(user_ids, title, message, data){

  Devices.find({user_id:{$in:[user_id]}}, function(err, success){
                var devices =[];

                _.forEach(success, function(data, err){

                devices.push(data.device.gcm_id);

             });

          var dataToPush = {title:title, message:message, type:"normal", data:data};

          console.log(devices);
          sendGCM(devices, dataToPush);

      });

}



function auctionPush(muffinId){


  MuffinForUsers.find({"muffinId":muffinId}, function(err, data){

    console.log(data);

    Devices.find({user_id:{$in:[data]}}, function(err, success){
                var devices =[];

                _.forEach(success, function(data, err){

                devices.push(data.device.gcm_id);

             });

          var dataToPush = {title:"Muffin", message:"Auction started", type:"auction", muffinId:muffinId};

          console.log(devices);
          sendGCM(devices, dataToPush);

      });


  }).select('userId').exec();

  

}


function invitePush(userIds, muffinId){

  Devices.find({user_id:{$in:[userIds]}}, function(err, success){
               var devices =[];

              _.forEach(success, function(data, err){

              devices.push(data.device.gcm_id);

           });

        var dataToPush = {title:"Muffin", message:"Muffin invitation", type:"invite", muffinId:muffinId};

        console.log(devices);
        sendGCM(devices, dataToPush);

    });

}



function testPush(){

  console.log('testPush');
  Devices.find({}, function(err, success){
                var devices =[];

                _.forEach(success, function(data, err){

                devices.push(data.device.gcm_id);

             });

          var muffinId = '57a88244fd6d830f414a528b';

          var dataToPush = {title:"heelo", message:"kjjqbwkq", type:"type", muffinId:muffinId};

          console.log(devices);
          sendGCM(devices, dataToPush);

      });

}






module.exports = {
  sendGCM: sendGCM,
  testPush: testPush,
  getTotalDelay: getTotalDelay,
  sendPush: sendPush,
  invitePush: invitePush,
  auctionPush: auctionPush,
  normalPush: normalPush
};
