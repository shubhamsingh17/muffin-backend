function hasSufficientParams(data, requiredKeys, specificValues, specificChecks) {
	var missing = [];
	var checkFunctions = {
		inArray: function (value, checkIn) {
			var boo = false;
			if (checkIn.indexOf(value) > -1) {
				boo = true;
			}
			return boo;
		},
		arrayLength: function (value, minLength) {
			var boo = false;
			if (value.length > minLength) {
				boo = true;
			}
			return boo;
		}
	};

	for (var i = requiredKeys.length - 1; i >= 0; i--) {
		if (typeof data[requiredKeys[i]] !== 'boolean') {
			if (!data[requiredKeys[i]]) {
				missing.push(requiredKeys[i]);
			}
		}
	}

	if (specificValues.length > 0) {
		for (i = specificValues.length - 1; i >= 0; i--) {
			var checkObj, currentValue;
			checkObj = specificChecks[i];
			currentValue = specificValues[i];

			if (!(checkFunctions[checkObj.checkType](data[currentValue], checkObj.checkAgainstValue))) {
				missing.push(currentValue);
			}
		}
	}

	if (missing.length > 0) {
		return {
			success: false,
			status: 400,
			msg: 'Insufficient parameter',
			data: missing
		};
	} else {
		return {
			success: true,
			msg: 'All required keys are present'
		};
	}
}



module.exports = {
	hasSufficientParams: hasSufficientParams
};