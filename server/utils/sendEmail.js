var querystring = require('querystring');
var https = require('https');

function sendElasticEmail(to, subject, body_text, body_html, from, fromName) {
  // Make sure to add your username and api_key below.
  var post_data = querystring.stringify({
    'username' : 'nishantt12@gmail.com',
    'api_key': '6152ba87-515a-4f25-ba49-20500956c27a',
    'from': 'hello@muffin.com',
    'from_name' : 'Muffin',
    'to' : 'nishantt21@gmail.com',
    'subject' : 'Welcome to Muffin!!!',
    'body_html' : '',
    'body_text' : '',
    'template':'muffin-email'
  });

  // Object of options.
  var post_options = {
    host: 'api.elasticemail.com',
    path: '/mailer/send',
    port: '443',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': post_data.length
    }
  };
  var result = '';
  // Create the request object.
  var post_req = https.request(post_options, function(res) {
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      result = chunk;
    });
    res.on('error', function (e) {
      result = 'Error: ' + e.message;
    });
  });

  // Post to Elastic Email
  post_req.write(post_data);
  post_req.end();
  return result;
}

sendElasticEmail('test@test.com', 'My Subject', 'My Text', 'My HTML', 'youremail@yourdomain.com', 'Your Name');
