var PDFDocument = require('pdfkit');
var fs = require('fs');

blobStream = require('blob-stream');



function pdfCreator() {

  console.log("end");
  
  doc = new PDFDocument;

  stream = doc.pipe(blobStream())

  doc.pipe(fs.createWriteStream(__dirname + '/out.pdf'));

  doc.fontSize(25)
    .text('Some text with an embedded font!', 100, 100)

  doc.end();

  console.log("end");
}



module.exports = {

  pdfCreator: pdfCreator

};
