var querystring = require('querystring');
var https = require('https');

var config = require('../config/environment');

function sendElasticEmail(to, subject, body_text, template) {
  // Make sure to add your username and api_key below.
  var post_data = querystring.stringify({
    'username': 'nishantt12@gmail.com',
    'api_key': '6152ba87-515a-4f25-ba49-20500956c27a',
    'from': 'hello@muffinapp.in',
    'from_name': 'MuffinApp',
    'to': to,
    'subject': subject,
    'body_html': '',
    'body_text': body_text,
    'template': template
  });

  // Object of options.
  var post_options = {
    host: 'api.elasticemail.com',
    path: '/mailer/send',
    port: '443',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': post_data.length
    }
  };
  var result = '';
  // Create the request object.
  var post_req = https.request(post_options, function (res) {
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      result = chunk;
    });
    res.on('error', function (e) {
      result = 'Error: ' + e.message;
    });
  });

  // Post to Elastic Email
  post_req.write(post_data);
  post_req.end();
  return result;
}


function welcome(to) {

  console.log(to);

  sendElasticEmail(to, "Welcome to Muffin!!!", "", "muffin-email");

}

function share(to, subject) {


  console.log(to + "   " + subject);

  sendElasticEmail(to, "Download Muffin!!!", subject, "");

}


function activity(subject) {

  sendElasticEmail(config.admin.email, "Activity!!!", subject, "");

}

function bankUpdate(subject) {

  console.log(config.admin.email);

  sendElasticEmail(config.admin.email, "Bank Update!!!", subject, "");

}


module.exports = {
  activity: activity,
  share: share,
  welcome: welcome,
  bankUpdate: bankUpdate
};


