var request = require('request');
var config = require('../config/environment');


// var formData = {
// 	"branch_key": config.branch.key_test,
// 	"data":{"event_id": event_id, "android_url": "market://details?id=com.twysk"}

// }

function getFormdata(muffinId, user_id, title) {
  var formData = {
    "branch_key": config.branch.key_live,
    "data": {
      "muffinId": muffinId,
      "user_id": user_id,
      "$android_url": "market://details?id=com.twysk",
      "$ios_url": "https://itunes.apple.com/in/app/id1067831727",
      "$og_title": title
    }

  }

  return formData;
}


function getOptions(muffinId, user_id, title) {
  var options = {
    uri: 'https://api.branch.io/v1/url',
    method: 'POST',
    json: getFormdata(muffinId, user_id, title)

  };

  return options;
}


function getUrl(muffinId, user_id, title, callback) {

  request(getOptions(muffinId, user_id, title), function (error, response, body) {

    callback(body.url);
    console.log(body.url + " response  " + response + "  error " + error + "  body  " + body);

    // if (!error && response.statusCode == 200) {
    //  	console.log(body.url) // Print the shortened url.

    // }

  });

};

module.exports = {

  getUrl: getUrl

};
