'use strict';

var express = require('express');
var chalk = require('chalk');
var config = require('./config/environment');
var mongoose = require('mongoose');
var cors = require('cors');

mongoose.connect(config.mongo.uri, config.mongo.options, function (error, data) {
  console.log('error', error);
  console.log('data', data);
});

var app = express();
var server = require('http').createServer(app);
var socket = require('socket.io')(server, {
  serveClient: true
});
app.use(cors());
app.options('*', cors());
require('./config/sockets.js')(socket);

require('./config/express')(app);
require('./routes')(app);



server.listen(config.port, config.ip, function () {

  console.log(
    chalk.red('\nExpress server listening on port ') + chalk.yellow('%d') + chalk.red(', in ') + chalk.yellow('%s') + chalk.red(' mode.\n'),
    config.port,
    app.get('env')
  );

  if (config.env === 'development') {
    require('ripe').ready();
  }

});




socket.sockets.on('connection', function (socket) {

    socket.connectDate = new Date();
    socket.ip = (socket.handshake.address) ? socket.handshake.address : null;

    console.log('[%s] %s logged.', socket.connectDate.toUTCString(), socket.ip);
    // sockets inserts
    require('/opt/muffin/server/api/activity/activity.socket.js').register(socket);
    require('/opt/muffin/server/api/company-docs/company-docs.socket.js').register(socket);
    require('/opt/muffin/server/api/auctions/auctions.socket.js').register(socket);
    require('/opt/muffin/server/api/auctions/main-auction.js').register(socket);
    require('/opt/muffin/server/api/muffins-for-users/muffins-for-users.socket.js').register(socket);
    require('/opt/muffin/server/api/muffin/muffin.socket.js').register(socket);

 

    socket.on('disconnect', function () {
      console.log('[%s] %s disconnected.', new Date().toUTCString(), socket.ip);
    });

    console.log('[%s] %s logged.', socket.connectDate.toUTCString(), socket.ip);

  });



module.exports = server;