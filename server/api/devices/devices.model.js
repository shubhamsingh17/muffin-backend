'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DevicesSchema = new Schema({
  user_id:{type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  device:{
    name: String,
    gcm_id: String,
    apns_id: String,
    android_id: String,
    ios_keychain_id: String,
  },

  created_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Devices', DevicesSchema);
