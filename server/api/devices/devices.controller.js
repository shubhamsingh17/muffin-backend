'use strict';

var _ = require('lodash');
var Devices = require('./devices.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Devices
 *
 * @param req
 * @param res
 */
exports.index = function (req, res) {
  Devices.find(function (err, devicess) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(devicess);
  });
};

/**
 * Get a single Devices
 *
 * @param req
 * @param res
 */
exports.show = function (req, res) {
  Devices.findById(req.params.id, function (err, devices) {
    if (err) { return handleError(res, err); }
    if (!devices) { return res.status(404).end(); }
    return res.status(200).json(devices);
  });
};

/**
 * Creates a new Devices in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function (req, res) {
  Devices.create(req.body, function (err, devices) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(devices);
  });
};

/**
 * Updates an existing Devices in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Devices.findById(req.params.id, function (err, devices) {
    if (err) { return handleError(res, err); }
    if (!devices) { return res.status(404).end(); }
    var updated = _.merge(devices, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(devices);
    });
  });
};

/**
 * Deletes a Devices from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function (req, res) {
  Devices.findById(req.params.id, function (err, devices) {
    if (err) { return handleError(res, err); }
    if (!devices) { return res.status(404).end(); }
    devices.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
