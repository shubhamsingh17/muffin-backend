var _ = require('lodash');
var MuffinsForUsers = require('../muffins-for-users/muffins-for-users.model');
var Muffins = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var AuctionsModel = require('../auctions/auctions.model');
var httpResponse = require('../../responses');
var Error = require('../../error');


exports.getMuffinsByType = function(req, res) {
	var whereQuery = {};
	if (req.params.type) whereQuery['state'] = req.params.type;
	Muffins.find(whereQuery, function(err, muffinsArray) {

		console.log(muffinsArray);

		return httpResponse.successResponse(res, muffinsArray);
	}).populate("createdBy").exec();
}



exports.getMuffindetail = function(req, res) {
	var whereQuery = {};
	var finalObject = {};
	whereQuery['_id'] = req.params.muffin_id;
	var userObject = [];
	Muffins.find(whereQuery, function(err, muffinDetail) {
		finalObject.muffin_details = muffinDetail[0];
		MuffinsForUsers.find({
			'muffinId': req.params.muffin_id
		}, function(err, muffinUserConfigs) {
			finalObject.users_details = []
			if (muffinUserConfigs.length < 1) return httpResponse.successResponse(res, finalObject);
			var userIds = _.pluck('userId', muffinUserConfigs);
			Users.find({
				'_id': {
					$in: userIds
				}
			}, function(err, userDetailArray) {
				var userDetailArray = _.indexBy('_id', userDetailArray);
				for (var i = 0; i < muffinUserConfigs; i++) {
					var muffinConfig = muffinUserConfigs[i].toObject();
					muffinConfig.userDetails = userDetails[muffinConfig['userId']];
					userObject.push(muffinConfig);
				}
				console.log(userDetailArray);
				return false;
				finalObject.users_details = userObject;
				return httpResponse.successResponse(res, finalObject);

			})
		})
	})
}

exports.changeMuffinState = function(req, res) {
	var whereQuery = {},
		options = {};
	var muffinId = req.params.muffin_id;
	var updatedState = req.body.updated_state;
	console.log('here it is the hit', req.body, req.params);
	if (!updatedState) return Error.errorMissingParam(res, 'updated_state');
	whereQuery['_id'] = muffinId;
	updateQuery = {
		'$set': {
			'state': updatedState
		}
	};
	updateMuffinsUtil(whereQuery, updateQuery, options, function(err, updateResults) {
		if (err) {
			return Error.errorCustom(res, err);
		}
		return httpResponse.successResponse(res, updateResults);
	});

}


exports.editMuffin = function(req, res) {
	var whereQuery = {},
		options = {};
	var muffinId = req.params.muffin_id;
	whereQuery['_id'] = muffinId;
	Muffins.findById(req.params.muffin_id, function(err, userDocument) {
		if (!userDocument) return handleError(res, 'No User found');
		var updated = _.merge(userDocument, req.body);
		var editedProfile = new Muffins(updated, {
			_id: false
		});
		editedProfile.save(function(errSave, data) {
			if (errSave) {
				console.log('here is error')
				return handleError(res, errSave);
			}
			return httpResponse.successResponse(res, data);
		})

	})

}


function updateMuffinsUtil(whereQuery, data, options, callback) {
	Muffins.findOneAndUpdate(whereQuery, data, options, function(err, object) {
		if (err) callback(err, false)
		if (object == null) return callback('No Data found', false);
		callback(null, true);
	});
}


function getMuffinByUserId(whereQuery, data, options, callback) {
	Muffins.find(whereQuery, data, options, function(err, object) {
		if (err) callback(err, false)
		if (object == null) return callback('no data with this id', false);
		callback(null, true);
	})
}