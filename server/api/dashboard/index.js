'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./controller');


router.get('/get/muffins/:type?', controller.getMuffinsByType);
router.get('/get/muffins/detail/:muffin_id', controller.getMuffindetail);
router.put('/change/muffin/state/:muffin_id', controller.changeMuffinState);
router.put('/edit/muffin/:muffin_id', controller.editMuffin)


module.exports = router;
