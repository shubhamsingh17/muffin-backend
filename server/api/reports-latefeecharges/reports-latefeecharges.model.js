'use strict';

var mongoose = require('mongoose');



var Schema = mongoose.Schema;

var ReportsLatefeechargesSchema = new Schema({

   userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  muffinId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Muffin'
  },
  auctionId:{
    type: mongoose.Schema.Types.ObjectId,
      ref: 'Auctions'
  },
  amountDue: {
    type: Number
  },
  amountPaid: {
    type: Number
  },
  auctionNum: {
    type: Number
  },
  date: {
    type: Date
  }



});

module.exports = mongoose.model('ReportsLatefeecharges', ReportsLatefeechargesSchema);