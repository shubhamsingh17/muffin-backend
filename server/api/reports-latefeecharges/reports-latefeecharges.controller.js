'use strict';

var _ = require('lodash');
var ReportsLatefeecharges = require('./reports-latefeecharges.model');
var Muffins = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var Auctions = require('../auctions/auctions.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var async = require('async');
var utils = require('../../utils/utilityFunctions.js');
var moment = require('moment');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Installmentpayable
 *
 * @param req
 * @param res
 */

exports.index = function(req, res) {
 ReportsLatefeecharges.find(function(err, data) {
       
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data

    });
  });
};

// late fee should be flexible -@todo
exports.latefee = function(req, res) {
  ReportsLatefeecharges.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    var calInstallment = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i].amountDue != 0 && data[i].amountPaid != 0) {
        calInstallment.push(data[i]);
      }
      var currentDate = moment();
      var firstLateFee = moment(calInstallment[i].date);
      var diffDate = currentDate.diff(firstLateFee, 'days') + 1;

      if (diffDate >= 10) {
        var noOfCharges = Math.floor((diffDate - 10) / 30);
        if (noOfCharges >= 1) {
          var firstLateCal = Math.floor(.02 * calInstallment[i].amountDue);
          firstLateCal = firstLateCal * (noOfCharges + 1);
        } else {
          var firstLateCal = Math.floor(.02 * calInstallment[i].amountDue);
        }
        calInstallment[i].amountDue = calInstallment[i].amountDue + firstLateCal;
      }

    }
    res.status(200).send({
      status: "success",
      response: calInstallment
    });

  });
};



/**
 * Get a single Installmentpayable
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  ReportsLatefeecharges.findById(req.params.id, function(err, data) {
    if (err) {
      return handleError(res, err);
    }
    if (!data) {
      return res.status(404).end();
    }
    return res.status(200).json(data);
  });
};

exports.findid = function(req, res) {
  console.log(req.body);
  console.log("inside late fee charges api");
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;

  if (!userId) {
    return Errors.errorMissingParam(res, 'userId');
  }
  if (!muffinId) {
    return Errors.errorMissingParam(res, 'muffinId');
  }  

  ReportsLatefeecharges.find({
    'userId': userId,
    'muffinId': muffinId
  }, function(err, data) {
    if (err) {
      console.log(err);
      return httpResponse.errorResponse(res, err, 422);
    }
    if (!data) {
      return httpResponse.errorResponse(res, err, 422);
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Creates a new Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
  ReportsLatefeecharges.create(req.body, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Updates an existing Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ReportsLatefeecharges.findById(req.params.id, function(err, data) {
    if (err) {
      return handleError(res, err);
    }
    if (!data) {
      return res.status(404).end();
    }
    var updated = _.merge(data, req.body);
    updated.save(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(data);
    });
  });
};

/**
 * Deletes a Installmentpayable from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  ReportsLatefeecharges.findById(req.params.id, function(err, data) {
    if (err) {
      return handleError(res, err);
    }
    if (!data) {
      return res.status(404).end();
    }
    ReportsLatefeecharges.remove(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).end();
    });
  });
};