'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReportsDividendReceivedSchema = new Schema({

  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  muffinId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Muffin'
  },
  auctionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Auctions'
  },
  transctionId: {
    type: String,
    minlength: 10,
    maxlength: 30
  },
  amountDue: {
    type: Number
  },
  dividendReceived: {
    type: Number
  },
  auctionNum: {
    type: Number
  },
  date: {
    type: Date
  }
});

module.exports = mongoose.model('ReportsDividendReceived', ReportsDividendReceivedSchema);
