'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./reports-dividendreceived.controller');
router.get('/', controller.index);
// router.get('/abc', controller.abc);
router.get('/:id', controller.show);
router.post('/new', controller.create);
router.post('/findbyid', controller.findid);

router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.get('/dividendReceived/:user_id', controller.getMyPaidAmount);
router.post('/amountDues', controller.getMyAmountBal);
router.post('/findData', controller.findData);

module.exports = router;

