'use strict';

var mongoose = require('mongoose');
var mongooseHistory = require('mongoose-history');
var Schema = mongoose.Schema;

var BankSchema = new Schema({
	bankName: {
		type: String,
		required: true
	},
	ifscCode: {
		type: String,
		required: true
	},
	branch: {
		type: String
	},
	address: {
		type: String
	},
	telephone: {
		type: Number
	},
	micrCode: {
		type: String
	},
	city: {
		type: String
	},
	distric: {
		type: String
	},
	state: {
		type: String
	}
});

module.exports = mongoose.model('Banks', BankSchema);