'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./bank.controller');
router.post('/newBank', controller.create); //create new bank
router.get('/:id', controller.showBank); //show single bank
router.get('/', controller.index); //show all banks
router.put('/:id', controller.update); //find by id and update one bank
router.delete('/:id', controller.delete); //find by id and delete one bank
router.post('/allSearchBanks', controller.allSearchBanks); // full search 
//insert bank from excel sheet[no api required]
module.exports = router;