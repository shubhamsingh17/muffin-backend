'use strict';

var _ = require('lodash');
var Bank = require('./bank.model');
var httpResponse = require('../../responses');
var Success = require('../../responses');
var Error = require('../../error');

function handleError(res, err) {
	return res.status(500).send(err);
}
/**
 * Get list of Banks
 *
 * @param req
 * @param res
 */

exports.index = function(req, res) {
	Bank.find(function(err, banks) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong while finding banks. Please try again.",
				errorInfo: err
			})
		}

		return res.status(200).send({
			status: "success",
			response: banks
		});
	});
};

/**
 * Get a single Bank
 *
 * @param req
 * @param res
 */
exports.showBank = function(req, res) {
	Bank.findById(req.params.id, function(err, data) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong check bank_id. Please try again.",
				errorInfo: err
			})
		}
		if (!data) {
			res.status(404).send({
				status: "error",
				message: "No Bank available here.",
				errorInfo: err
			})
		}
		return res.status(200).send({
			status: "success",
			response: data
		});
	});
};

/**
 * Creates a new Bank in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
	Bank.create(req.body, function(err, bank) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "We can't craete Bank Please Fill required field. Please try again.",
				errorInfo: err
			})
		}
		return res.status(200).send({
			status: "success",
			response: bank
		});
	});
};

/**
 * Get list of Banks
 *
 * @param req
 * @param res
 
 
 */

function findBankAll(whereQuery, selectedOptions, limit, sortBy, skip, cb) {
	if (!limit) limit = 30;
	if (!sortBy) sortBy = {
		'bankName': 1,
		'state': -1
	};
	if (!skip) skip = 0;

	Bank.find(whereQuery, selectedOptions).limit(limit).sort(sortBy)
		.exec(function(err, banks) {
			cb(null, banks)
		});

}

/**
 * Get Single Bank
 *
 * @param req
 * @param res
 
 
 */

exports.show = function(req, res) {
		Bank.findById(req.params.id, function(err, bank) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Please try again.",
					errorInfo: err
				});
			}
			if (!bank) {
				res.status(404).send({
					status: "error",
					message: "No Bank available here.",
					errorInfo: err
				});
			}
			return res.status(200).send({
				status: "success",
				response: bank
			})
		})
	}
	/**
	 * Get Single Bank and Update
	 *
	 * @param req
	 * @param res
	 
	 
	 */

function updateOneBank(whereQuery, data, options, callback) {
	Bank.findOneAndUpdate(whereQuery, data, options, function(err, object) {

		if (err) callback(err, false)
		if (object == null) return callback('No Data found', false);
		callback(null, object);
	});
}


/**
 * Find Banks
 *
 * @param req
 * @param res
 
 
 */

exports.findBanks = function(req, res) {

	var whereQuery = {},
		selectedOptions = {},
		limit = 30,
		page = 0,
		sortBy, skip;

	if (req.params.bank_id) whereQuery['_id'] = req.params.bank_id;
	if (req.params.state) whereQuery['state'] = req.params.state;
	if (req.params.limit) limit = req.params.limit;
	if (req.params.page) page = req.params.page;


	findBankAll(whereQuery, selectedOptions, limit, sortBy, skip, function(err, banks) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Please try again.",
				errorInfo: err
			})
		}
		return httpResponse.successResponse(res, banks);
	});

};

/**
 * Updates an existing Banks in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
	if (req.body._id) {
		delete req.body._id;
	}
	Bank.findById(req.params.id, function(err, banks) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Please try again.",
				errorInfo: err
			})
		}
		if (!banks) {
			res.status(404).send({
				status: "error",
				message: "No Bank available here.",
				errorInfo: err
			})
		}
		var updated = _.merge(banks, req.body);
		updated.save(function(err) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Please try again.",
					errorInfo: err
				})
			}
			return res.status(200).send({
				status: "success",
				response: banks
			});
		});
	});
};

/**
 * Deletes a Bank from the DB.
 *
 * @param req
 * @param res
 */
exports.delete = function(req, res) {
	Bank.findById(req.params.id, function(err, bank) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Please try again.",
				errorInfo: err
			})
		}
		if (!bank) {
			res.status(404).send({
				status: "error",
				message: "No Bank available here.",
				errorInfo: err
			})
		}
		bank.remove(function(err) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Please try again.",
					errorInfo: err
				})
			}
			return res.status(200).send({
				status: "success",
				message: "Successfully deleted record"

			});
		});
	});
};

/**
 * Find all Banks from the DB with text search.
 *
 * @param req
 * @param res
 */


function escapeRegex(text) {
	return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};

// Search banks
exports.allSearchBanks = function(req, res) {
	try {
		if (req.body.query && req.body.query != "") {

			var limit = 12;

			var search = new RegExp(escapeRegex(req.body.query), 'gi');

			var orQueries = [{
				bankName: new RegExp(search, 'i')
			}, {
				ifscCode: new RegExp(search, 'i')
			}, {
				city: new RegExp(search, 'i')
			}, {
				state: new RegExp(search, 'i')
			}];

			Bank.find({

					$or: orQueries

				})
				.limit(limit)

			.exec(function(err, banks) {
				if (err) {
					res.status(200).send({
						status: "error",
						message: "Something went wrong while searching banks. Please try again.",
						errorInfo: err
					});
				} else {
					res.status(200).send({
						status: "success",
						response: banks
					});
				}

			});
		} else {
			res.status(200).send({
				status: "error",
				message: "Search term required."
			});
		}
	} catch (err) {
		res.status(500).send({
			status: "error",
			message: "Something went wrong while searching banks. Please try again."
		});
	}
}