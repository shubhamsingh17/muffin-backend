'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContactUsSchema = new Schema({

  userName: String,
  userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  userEmail: String,

  topic: String,
  message: String,

  serverMessage: {type: String, default: "We will contact you with in 24 hours, Thanks"}


});

module.exports = mongoose.model('ContactUs', ContactUsSchema);
