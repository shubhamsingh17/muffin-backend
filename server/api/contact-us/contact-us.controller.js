'use strict';

var _ = require('lodash');
var ContactUs = require('./contact-us.model');

var httpResponse = require('../../responses');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of ContactUs
 *
 * @param req
 * @param res
 */
exports.index = function (req, res) {
  ContactUs.find(function (err, contactUss) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(contactUss);
  });
};

/**
 * Get a single ContactUs
 *
 * @param req
 * @param res
 */
exports.show = function (req, res) {
  ContactUs.findById(req.params.id, function (err, contactUs) {
    if (err) {
      return handleError(res, err);
    }
    if (!contactUs) {
      return res.status(404).end();
    }
    return res.status(200).json(contactUs);
  });
};

/**
 * Creates a new ContactUs in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function (req, res) {
  ContactUs.create(req.body, function (err, contactUs) {
    if (err) {
      httpResponse.errorResponse(res, "Try again later", 500, "Try again later");
    }
    return httpResponse.successResponse(res, contactUs);

  });
};

/**
 * Updates an existing ContactUs in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ContactUs.findById(req.params.id, function (err, contactUs) {
    if (err) {
      return handleError(res, err);
    }
    if (!contactUs) {
      return res.status(404).end();
    }
    var updated = _.merge(contactUs, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(contactUs);
    });
  });
};

/**
 * Deletes a ContactUs from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function (req, res) {
  ContactUs.findById(req.params.id, function (err, contactUs) {
    if (err) {
      return handleError(res, err);
    }
    if (!contactUs) {
      return res.status(404).end();
    }
    contactUs.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).end();
    });
  });
};
