'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./reports-amountpaid.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/new', controller.create);
router.post('/findbyid', controller.findid);

router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.get('/amountPaid/:user_id', controller.getMyPaidAmount);
router.post('/amountDues', controller.getMyAmountBal);
router.post('/findData', controller.findData);

module.exports = router;