'use strict';

var _ = require('lodash');
var ReportsAmountpaid = require('./reports-amountpaid.model');
var Muffins = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var Auctions = require('../auctions/auctions.model');
var ReportsBouncecharges = require('../reports-bouncecharges/reports-bouncecharges.model');
var ReportsDelayedpaymentinterest = require('../reports-delayedpaymentinterest/reports-delayedpaymentinterest.model');
var ReportsInstallmentpayable = require('../reports-installmentpayable/reports-installmentpayable.model');
var ReportsLatefeecharges = require('../reports-latefeecharges/reports-latefeecharges.model');
var Dividend=require('../reports-dividendreceived/reports-dividendreceived.model');
var Reports = require('../reports/report.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var async = require('async');
var utils = require('../../utils/utilityFunctions.js');
var moment = require('moment');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Installmentpayable
 *
 * @param req
 * @param res
 */


exports.index = function(req, res) {
  ReportsAmountpaid.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Get a single Installmentpayable
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  ReportsAmountpaid.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(200).send({
        status: "success",
        message: 'nothing available'
      });
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

exports.findid = function(req, res) {
  console.log(req.body);
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;

  if (!userId) {
    return Errors.errorMissingParam(res, 'userId');
  }
  if (!muffinId) {
    return Errors.errorMissingParam(res, 'muffinId');
  }

  ReportsAmountpaid.find({
    'userId': userId,
    'muffinId': muffinId
  }, function(err, data) {
    if (err) {
      console.log(err);
      return httpResponse.errorResponse(res, err, 422);
    }
    if (!data) {
      return httpResponse.errorResponse(res, err, 422);
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Creates a new Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
  ReportsAmountpaid.create(req.body, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Updates an existing Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ReportsAmountpaid.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    var updated = _.merge(data, req.body);
    updated.save(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        response: data
      });
    });
  });
};

/**
 * Deletes a Installmentpayable from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  ReportsAmountpaid.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    ReportsAmountpaid.remove(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        message: "Successfully deleted.",

      })
    });
  });
};


exports.getMyPaidAmount = function(req, res) {
  var userId = req.params.user_id;

  ReportsAmountpaid.find({
    userId: userId,
    amountPaid: {
      $gte: 0
    }
  }, function(err, getMyPaidAmount) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      res: getMyPaidAmount

    })
  })
}

exports.getMyAmountBal = function(req, res) {
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;
  if (userId && muffinId) {
    ReportsAmountpaid.findOne({
      'userId': userId,
      'muffinId': muffinId
    }, function(err, amount) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        });
      } else if (amount) {

        var outObject = {};
        outObject.amountDue = amount.amountDue;
        outObject.amountPaid = amount.amountPaid;
        outObject.amountBalance = Math.floor(outObject.amountDue - outObject.amountPaid);
        outObject.auctionId = amount.auctionId;
        outObject.auctionNum = amount.auctionNum;
        outObject.muffinId = amount.muffinId;
        outObject.userId = amount.userId;
        outObject._id = amount._id;
        res.status(200).send({
          status: "success",
          res: outObject
        });
      } else {
        res.status(200).send({
          error: 'No Data Found'
        });
      }
    });
  } else {
    res.status(200).send({
      error: 'No User Id Provided'
    });
  }
};

exports.amountBal = function(req, res) {
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;

  ReportsBouncecharges.findOne({
    "userId": userId,
    "muffinId": muffinId
  }, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      });
    } else if (data) {
      var bounceCharges = {};
      bounceCharges.amountDue = data.amountDue;
      bounceCharges.amountPaid = data.amountPaid;
      bounceCharges.amountBalance = Math.floor(bounceCharges.amountDue - bounceCharges.amountPaid);
      bounceCharges.transctionId = data.transctionId;
      if (!bounceCharges.transctionId) {
        bounceCharges.amountBalance = bounceCharges.amountBalance + 100;
      }

      ReportsLatefeecharges.findOne({
        "userId": userId,
        "muffinId": muffinId
      }, function(err, data) {
        if (err) {
          res.status(200).send({
            status: "error",
            message: "Something went wrong, Please try again.",
            errorInfo: err
          });
          console.log(err);
        } else if (data) {
          console.log(data);
          return false;
          var lateFee = {};

        }
      })
    }
  })
}

exports.findData = function(req, res, callback) {

  var userId = req.body.userId;
  var muffinId = req.body.muffinId;
  var auctionId = req.body.auctionId;
  async.parallel({
    ReportsBouncecharges: function(callback) {
      return Reports.findOne({
        "userId": userId,
        "muffinId": muffinId
      }, function(err, result) {
        // console.log(result);
        // return false;
        if (err) {
          callback(err);
        } else {
          if (result == null) {
            callback(null, 0);
          } else {
            if (result.transctionId == null) {
              var amount = 100;

            } else {
              var amount = 0;
            }
            callback(null, amount);
          }
        }
      });
    },
    ReportsLatefeecharges: function(callback) {
      return ReportsLatefeecharges.findOne({
        "userId": userId,
        "muffinId": muffinId
      }, function(err, result) {
        if (err) {
          callback(err);
        }
        if (result == null) {
          callback(null, 0)
        } else {

          if (result.amountDue != 0 && result.amountPaid != 0) {
            var currentDate = moment();
            var firstLateFee = moment(result.date);
            var diffDate = currentDate.diff(firstLateFee, 'days') + 1;

            if (diffDate >= 10) {
              var noOfCharges = Math.floor((diffDate - 10) / 30);
              if (noOfCharges >= 1) {
                var firstLateCal = Math.floor(.02 * result.amountDue);
                firstLateCal = firstLateCal * (noOfCharges + 1);
              } else {
                var firstLateCal = Math.floor(.02 * result.amountDue);
              }
              var amount = firstLateCal;
            } else {
              var amount = 0;
            }
          }
          callback(null, amount);
        }

      });
    },
    ReportsInstallmentpayable: function(callback) {
      return Auctions.findById(auctionId)
        .populate('muffinId')
        .exec(function(err, data) {
          if (err) {
            callback(err);
          }
          if (data == null) {
            callback(null, 0);
          } else {
            var faceValue = data.members * data.instalment;
            var allInst = Math.floor((.05 * faceValue) / 24 + (data.highestBid.bid / 24));
            callback(null, allInst);
          }

        });
    },
    ReportsDelayedpaymentinterest: function(callback) {
      return ReportsDelayedpaymentinterest.findOne({
        "userId": userId,
        "muffinId": muffinId
      }, function(err, result) {
        if (err) {
          callback(err);
        } else {
          if(result == null){
            callback(null, 0);
          }else{
            if (result.amountDue != 0 && result.amountPaid != 0) {
            var currentDate = moment();

            var firstLateFee = moment(result.date);

            var diffDate = currentDate.diff(firstLateFee, 'days') + 1;

            if (diffDate >= 10) {
              var noOfCharges = Math.floor((diffDate - 10) / 30);
              if (noOfCharges >= 1) {
                var firstLateCal = Math.floor(.035 * result.amountDue);
                firstLateCal = firstLateCal * (noOfCharges + 1);
              } else {
                var firstLateCal = Math.floor(.035 * result.amountDue);
              }
              var amount = firstLateCal;
            } else {
              var amount = 0;
            }
          }

          callback(null, amount);
          }

        }
      });
    },
    ReportsAmountpaid: function(callback) {
      return ReportsAmountpaid.findOne({
        "userId": userId,
        "muffinId": muffinId
      }, function(err, result) {
        if (err) {
          callback(err);
        } else {
          if (result == null) {
            callback(null, 0);
          } else {
            var amount = Math.floor(result.amountDue - result.amountPaid);
            callback(null, amount);
          }
        }
      })
    }
  }, function(err, totalAmounts) {
    if (err) {
      return res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again."
      });
    } else {
      var totalCal = totalAmounts.ReportsAmountpaid + totalAmounts.ReportsBouncecharges + totalAmounts.ReportsDelayedpaymentinterest + totalAmounts.ReportsLatefeecharges + totalAmounts.ReportsInstallmentpayable;
      return res.status(200).send({
        "status": "success",
        "res": {
          "amount": totalCal
        }
      });
    }

  });

}