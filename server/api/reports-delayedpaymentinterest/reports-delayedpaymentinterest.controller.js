'use strict';

var _ = require('lodash');
var ReportsDelayedpaymentinterest = require('./reports-delayedpaymentinterest.model');
var Muffins = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var Auctions = require('../auctions/auctions.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var async = require('async');
var utils = require('../../utils/utilityFunctions.js');
var moment = require('moment');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Installmentpayable
 *
 * @param req
 * @param res
 */
exports.index = function(req, res) {
  ReportsDelayedpaymentinterest.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};


exports.delayfee = function(req, res) {
  ReportsDelayedpaymentinterest.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    var calDelayfee = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i].amountDue != 0 && data[i].amountPaid != 0) {
        calDelayfee.push(data[i]);
      }
      var currentDate = moment();

      var firstLateFee = moment(calDelayfee[i].date);

      var diffDate = currentDate.diff(firstLateFee, 'days') + 1;

      if (diffDate >= 10) {
        var noOfCharges = Math.floor((diffDate - 10) / 30);
        if (noOfCharges >= 1) {
          var firstLateCal = Math.floor(.035 * calDelayfee[i].amountDue);
          firstLateCal = firstLateCal * (noOfCharges + 1);
        } else {
          var firstLateCal = Math.floor(.035 * calDelayfee[i].amountDue);
        }
        calDelayfee[i].amountDue = calDelayfee[i].amountDue + firstLateCal;
      }
       
    }
    res.status(200).send({
      status: "success",
      response: calDelayfee
    });

  });
};

exports.findid = function(req, res) {
  console.log(req.body);
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;

  if (!userId) {
    return Errors.errorMissingParam(res, 'userId');
  }
  if (!muffinId) {
    return Errors.errorMissingParam(res, 'muffinId');
  }  

  ReportsDelayedpaymentinterest.find({
    'userId': userId,
    'muffinId': muffinId
  }, function(err, data) {
    if (err) {
      console.log(err);
      return httpResponse.errorResponse(res, err, 422);
    }
    if (!data) {
      return httpResponse.errorResponse(res, err, 422);
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

exports.index = function(req, res) {
  ReportsDelayedpaymentinterest.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Get a single Installmentpayable
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  ReportsDelayedpaymentinterest.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Creates a new Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
  ReportsDelayedpaymentinterest.create(req.body, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Updates an existing Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ReportsDelayedpaymentinterest.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    var updated = _.merge(data, req.body);
    updated.save(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        response: data
      });
    });
  });
};

/**
 * Deletes a Installmentpayable from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  ReportsDelayedpaymentinterest.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    ReportsDelayedpaymentinterest.remove(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        message: "Successfully deleted."

      })
    });
  });
};