'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StatementsSchema = new Schema({
  muffin_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Muffin'},
  user_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  auction_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Auctions'},

  amountPayable: Number,
  created_at: {type: Date, default: Date.now},
  amountReceivable: Number,
  amountPaid: Number,
  lateFee: Number,
  balance: Number,
  paymentDeclined: Number


});

module.exports = mongoose.model('Statements', StatementsSchema);
