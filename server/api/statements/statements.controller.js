'use strict';

var _ = require('lodash');
var Statements = require('./statements.model');
var httpResponse = require('../../responses');

function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Statements
 *
 * @param req
 * @param res
 */
exports.index = function (req, res) {
  Statements.find(function (err, statementss) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(statementss);
  });
};

/**
 * Get a single Statements
 *
 * @param req
 * @param res
 */
exports.show = function (req, res) {
  Statements.findById(req.params.id, function (err, statements) {
    if (err) {
      return handleError(res, err);
    }
    if (!statements) {
      return res.status(404).end();
    }
    return res.status(200).json(statements);
  });
};

/**
 * Creates a new Statements in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function (req, res) {
  Statements.create(req.body, function (err, statements) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(statements);
  });
};

/**
 * Updates an existing Statements in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Statements.findById(req.params.id, function (err, statements) {
    if (err) {
      return handleError(res, err);
    }
    if (!statements) {
      return res.status(404).end();
    }
    var updated = _.merge(statements, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(statements);
    });
  });
};

/**
 * Deletes a Statements from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function (req, res) {
  Statements.findById(req.params.id, function (err, statements) {
    if (err) {
      return handleError(res, err);
    }
    if (!statements) {
      return res.status(404).end();
    }
    statements.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).end();
    });
  });
};


exports.getStatements = function (req, res) {

  var muffinId = req.params.muffinId;
  var userId = req.params.userId;

  Statements.find({"muffin_id": muffinId, "user_id": userId}, function (err, transactions) {

    return httpResponse.successResponse(res, transactions);
  });
};
