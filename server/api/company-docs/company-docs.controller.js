'use strict';

var _ = require('lodash');
var CompanyDocs = require('./company-docs.model');
var DownloadFile = require('../../utils/downloadFile');
// var testingPdf = require('../../utils/testingPdf');

function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of CompanyDocs
 *
 * @param req
 * @param res
 */
exports.index = function (req, res) {
  CompanyDocs.find(function (err, companyDocss) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(companyDocss);
  });
};

/**
 * Get a single CompanyDocs
 *
 * @param req
 * @param res
 */
exports.show = function (req, res) {
  CompanyDocs.findById(req.params.id, function (err, companyDocs) {
    if (err) {
      return handleError(res, err);
    }
    if (!companyDocs) {
      return res.status(404).end();
    }
    return res.status(200).json(companyDocs);
  });
};

/**
 * Creates a new CompanyDocs in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function (req, res) {
  CompanyDocs.create(req.body, function (err, companyDocs) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(companyDocs);
  });
};

/**
 * Updates an existing CompanyDocs in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  CompanyDocs.findById(req.params.id, function (err, companyDocs) {
    if (err) {
      return handleError(res, err);
    }
    if (!companyDocs) {
      return res.status(404).end();
    }
    var updated = _.merge(companyDocs, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(companyDocs);
    });
  });
};

/**
 * Deletes a CompanyDocs from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function (req, res) {
  CompanyDocs.findById(req.params.id, function (err, companyDocs) {
    if (err) {
      return handleError(res, err);
    }
    if (!companyDocs) {
      return res.status(404).end();
    }
    companyDocs.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).end();
    });
  });
};


exports.downloadFiles = function (req, res) {

  console.log("download files");

  testingPdf.downloadFile(res);

};

exports.check = function (req, res) {

  console.log("download files");

  DownloadFile.downloadFile(res);

  // return res.status(204).end();

};
