'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./company-docs.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/downloadFileshvsjsd', controller.downloadFiles);
router.get('/download/:auctionId/:transactionId', controller.downloadFiles);

router.post('/', controller.create);

router.put('/:id', controller.update);

router.delete('/:id', controller.destroy);

module.exports = router;
