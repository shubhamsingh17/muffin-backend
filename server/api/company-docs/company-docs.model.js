'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CompanyDocsSchema = new Schema({
  
  doc: {

  },

  amountInCompany: Number,

  commision:[{

    muffinId : {type: mongoose.Schema.Types.ObjectId, ref: 'Muffin'},
    auctionId: {type: mongoose.Schema.Types.ObjectId, ref: 'Auctions'},
    userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    amount: Number

  }],

  bank:[{

      active: Boolean,
      bankName: String,
      bankAcNumber: String,
      bankBranch: String,
      bankIfsc: String,
        
    }]

});

module.exports = mongoose.model('CompanyDocs', CompanyDocsSchema);
