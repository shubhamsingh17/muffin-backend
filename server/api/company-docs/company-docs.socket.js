'use strict';

var CompanyDocs = require('./company-docs.model');

exports.register = function (socket) {

  CompanyDocs.schema.post('save', function (doc) {
    socket.emit('CompanyDocs:save', doc);
  });

  CompanyDocs.schema.post('remove', function (doc) {
    socket.emit('CompanyDocs:remove', doc);
  });

};
