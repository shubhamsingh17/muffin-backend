'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./user.controller');
var auth = require('../../auth/auth.service');

router.get('/me', auth.isAuthenticated(), controller.getMe);
router.post('/create', controller.create);
router.post('/userExists', controller.userExists);
router.post('/verifyOtp', controller.verifyOtp);
router.post('/resendOtp', controller.resendOtp);
router.post('/createOtp', controller.createOtp);
router.post('/login', controller.login);
router.post('/shareApp', controller.shareApp);

router.post('/loginUserName', controller.loginUserName);
router.post('/setPasswordPin', controller.setPasswordPin);
router.put('/edit/my/profile/bank/:user_id', controller.editMyBank);

router.post('/setPin', controller.setPin);
router.get('/getVideos', controller.getVideos);
router.post('/addDevice', controller.updateDevice);
router.post('/amountdue', controller.amountDue);

router.get('/get/my/profile/:user_id', auth.isAuthenticated(), controller.getMyProfile); //@todo have to authenticate the same from auth
router.put('/edit/my/profile/:user_id', auth.isAuthenticated(), controller.editMyProfile); //@todo have to authenticate the same from auth

router.get('/get/profile/:user_id', controller.getProfile); //@todo have to authenticate the same from auth
router.put('/edit/profile', controller.editProfile);


router.get('/get/all/users/:user_id?/:user_email?/:limit?/:page?', controller.getUsers);
router.get('/get/allUsers/:limit', controller.getAllUsers);
router.get('/isKycCaDone', controller.isKycCaDone);
router.get('/userList', controller.index);
module.exports = router;