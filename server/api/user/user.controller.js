'use strict';

var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var User = require('./user.model');
var Promise = require('bluebird');
var _ = require('lodash');
var Errors = require('../../error');
var Success = require('../../responses');
var Email = require('../../utils/emailSender');

var FB = require('fb');

FB.options({
  'appSecret': 'b9a21edb12f07c141f3a52a91e237a65'
});
FB.options({
  'appId': '467653353577660'
});


var Promise = require('bluebird');
var OtpController = require('../one_time_password');
var httpResponse = require('../../responses');
var Devices = require('../devices/devices.model');
var utils = require('../../utils/utilityFunctions.js');

var UserObj = Promise.promisifyAll(User);


function handleError(res, err) {
  return res.status(500).send(err);
}


function checkFind(email) {
  return User.findOne(email);
}


function checkFindUserName(data) {
  return User.findOne(data);
}

function createUser(user, res) {
  User.create(user, function(err, user) {
    if (err) {
      Success.errorResponse(res, 'Validation Error', 401, err.errors[Object.keys(err.errors)[0]].message);
    } else {
      var token = jwt.sign({
          _id: user._id
        },
        config.secrets.session, {
          expiresInMinutes: 10 * 365 * 24 * 60
        }
      );
      createOtp(user._id, user.phone, function(otp) {

        var response = {
          'otp': otp,
          'token': token,
          'user': user
        };
        Email.welcome(user.email);

        return Success.successResponse(res, response, 200);

      });


    }
  });
}


function fbVerification(user, res, accessToken) {
  FB.setAccessToken(accessToken);
  FB.api('/me', function(data) {
    if (data && data.error) {
      if (data.error.code === 'TIMEDOUT') {
        return Errors.errorCustom(res, 'FACEBOOK TOKEN TIMEOUT');
      } else {
        return Errors.errorCustom(res, data.error);
      }
    } else {
      user.fbId = data.id;
      user.name = data.name;
      user.gender = data.gender;
      return createUser(user, res);
    }
  });
}


function createOtp(userId, phone, callBack) {
  var requiredFields = ['user_id', 'phone'];
  var paramCheck = utils.hasSufficientParams({
    user_id: userId,
    phone: phone
  }, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    UserObj.findOneAsync({
        '_id': userId
      })
      .then(function(user) {
          if (user) {
            // if ( isResend ) return OtpController.resendOtp( req, res, user );
            OtpController.createOtp(phone, callBack);
          } else return Errors.errorDBNotFound(res, 'User');
        },
        function(err) {
          console.log(err);
          Errors.errorServer(res, err);
        });
  }
}

function findUsersUtil(whereQuery, selectedOptions, limit, sortBy, skip, cb) { //util for findUsers

  limit = limit || 10;
  sortBy = sortBy || '-created_at';
  skip = skip || 0;

  User.find(whereQuery, selectedOptions).limit(limit).sort(sortBy)
    .exec(function(err, users) {
      cb(null, users);
    });

}


function updateUsersUtil(whereQuery, data, options, callback) {
  User.findOneAndUpdate(whereQuery, data, options, function(err, object) {
    if (err) callback(err, false);
    if (object == null) return callback('No Data found', false);
    callback(null, true);
  });
}
exports.create = function(req, res) {

  var user = req.body;

  var email = req.body.email;
  var fbToken = req.body.fbToken;
  var password = req.body.password;
  var username = req.body.username;
  var phone = req.body.phone;


  var paramCheck = utils.hasSufficientParams(req.body, ['email', 'phone', 'username'], [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    if (!fbToken && !password) {
      return Errors.errorMissingParam(res, 'password missing');
    } else {
      if (fbToken) {
        fbVerification(user, res, fbToken);
        // createUser(user, res);
      } else {
        createUser(user, res);
      }
    }
  }

};


exports.shareApp = function(req, res) {


  var email = req.body.email;

  var message = req.body.message + '\n' + 'Link: https://goo.gl/h8xUkw';


  Email.share(email, message);

  Success.successResponse(res, {
    message: 'done'
  }, 200);

};


exports.login = function(req, res) {
  var email = req.body.email;
  var fbToken = req.body.fbToken;
  var password = req.body.password;
  var paramCheck = utils.hasSufficientParams(req.body, ['email'], [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    if (!fbToken && !password) {
      return Errors.errorMissingParam(res, 'password');
    } else {
      checkFind(email)
        .then(function(user) {
          console.log(user);
          if (!user) {

            Success.errorResponse(res, 'User not present', 401, 'User not present');

          } else {

            var token = jwt.sign({
                _id: user._id
              },
              config.secrets.session, {
                expiresInMinutes: 10 * 365 * 24 * 60
              }
            );

            console.log('this is token', token)

            var response = {
              'token': token,
              'user': user
            };
            return Success.successResponse(res,
              response, 200);


          }
        });
    }
  }

};

exports.setPasswordPin = function(req, res) {
  var requiredFields = ['username', 'password', 'pin'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    var username = req.body.username;
    var password = req.body.password;
    var pin = req.body.pin;
    checkFindUserName({
      username: username
    }).then(function(user) {
      console.log('user');
      console.log(user);
      if (!user) {
        Success.errorResponse(res, 'User not present', 401, 'User not present');
      } else {
        User.findOneAndUpdate({
          '_id': user._id
        }, {
          'password': password,
          'pin': pin
        }, {
          new: true
        }, function(error, updateResult) {
          if (error) {
            console.error('error');
            console.error(error);
            Success.errorResponse(res, 'Something went wrong', 500, 'Something went wrong');
          } else {
            console.log("_____Update Result for reset Password / PIN is ________ \n")
            console.log(updateResult);
            var response = {
              'message': 'done',
              'user': updateResult
            };
            Success.successResponse(res, response, 200);
          }
        });
      }
    }).catch(function(error) {
      console.error('error');
      console.error(error);
      Success.errorResponse(res, 'Something went wrong', 500, 'Something went wrong');
    });
  }
};


exports.loginUserName = function(req, res) {
  var requiredFields = ['password', 'username'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);

  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {

    var password = req.body.password;
    var fbToken = req.body.fbToken;
    var username = req.body.username;
    checkFindUserName({
        username: username,
        password: password
      })
      .then(function(user) {
        console.log('users' + user + '   ');
        if (!user) {
          Success.errorResponse(res, 'User not present', 401, 'User not present');
        } else {
          var token = jwt.sign({
              _id: user._id
            },
            config.secrets.session, {
              expiresInMinutes: 10 * 365 * 24 * 60
            }
          );

          var response = {
            'token': token,
            'user': user
          };
          return Success.successResponse(res,
            response, 200);


        }
      }).catch(function(error) {
        console.error('error');
        console.error(error);
        return cb(null, {
          success: false,
          msg: 'Something went wrong',
          data: error
        });
      });
  }
};

exports.resendOtp = function(req, res) {
  var requiredFields = ['user_id', 'phone'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    var userId = req.body.user_id;
    var phone = req.body.phone;
    UserObj.findOneAsync({
        '_id': userId
      })
      .then(function(user) {

        if (!user) return Errors.errorDBNotFound(res, 'User');

        else {
          console.log(phone);
          return OtpController.resendOtp(req, res, phone);
        }
      })
      .error(function(err) {
        // email_scheduler.error_mail( err );
        return Errors.errorServer(res, err);
      });
  }
};


exports.createOtp = function(req, res) {
  var requiredFields = ['user_id', 'phone'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    var userId = req.body.user_id;
    UserObj.findOneAsync({
        '_id': userId
      })
      .then(function(user) {
          if (user) {
            // if ( isResend ) return OtpController.resendOtp( req, res, user );
            OtpController.createOtp(phone, function(otp) {

              return Success.successResponse(res, {
                otp: otp
              }, 200);

            });
          } else return Errors.errorDBNotFound(res, 'User');
        },
        function(err) {
          Errors.errorServer(res, err);
        });
  }
};


exports.userExists = function(req, res) {
  var requiredFields = ['email'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    var email = req.body.email;
    console.log(email);
    checkFind({
      email: email
    }).then(function(user) {
      console.log(user);
      if (!user) {
        return Success.successResponse(res, {
          'new_user': true
        }, 200);
      } else {
        var data = {
          "user": user,
          "isUserExists": true
        }
        Success.successResponse(res, data, 200);

      }
    });
  }
};


exports.verifyOtp = function(req, res) {
  var requiredFields = ['user_id', 'phone'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    var phone = req.body.phone;
    var userId = req.body.user_id;
    // return OtpController.verifyOtpPromise(phone, otpClaim)
    //   .then(function (verified) {
    // if (!verified) {
    //   Success.errorResponse(res, 'Incorrect otp', 500, 'Incorrect otp');
    // } else {
    User.update({
      _id: userId
    }, {
      $set: {
        'phone': phone,
        'is_phone_verified': true
      }
    }, function(err, user) {
      if (err) {

      }
      return Success.successResponse(res, {
        message: 'done'
      }, 200);
    });
    //}
    // });
  }

};


/**
 * Return the current logged user.
 *
 * @param req
 * @param res
 */

exports.getMe = function(req, res) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -passwordHash', function(err, user) {
    if (err) {
      return handleError(res, err);
    }
    if (!user) {
      return res.json(401);
    }
    res.status(200).json(user);
  });
};


exports.setPin = function(req, res) {
  var requiredFields = ['username', 'pin'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    var username = req.body.username;
    var pin = req.body.pin;
    User.update({
      username: username
    }, {
      $set: {
        pin: pin
      }
    }, function(err, user) {
      if (err) {
        console.log(err);
        Success.errorResponse(res, 'Something went wrong', 500, 'Failed to update pin');
      } else {
        Success.successResponse(res, {
          message: 'done'
        }, 200);
      }
    });
  }
};

exports.getMyProfile = function(req, res) {
  var whereQuery = {},
    selectedOptions = {},
    limit = 10,
    page = 0,
    sortBy, skip;

  if (req.params.user_id) whereQuery['_id'] = req.user._id;

  findUsersUtil(whereQuery, selectedOptions, limit, sortBy, skip, function(err, users) {
    if (err) {
      return handleError(res, err);
    }
    return httpResponse.successResponse(res, users);
  });

};

exports.getProfile = function(req, res) {
  var whereQuery = {},
    selectedOptions = {},
    limit = 10,
    page = 0,
    sortBy, skip;

  whereQuery['_id'] = req.params.user_id;

  findUsersUtil(whereQuery, selectedOptions, limit, sortBy, skip, function(err, users) {
    if (err) {
      return handleError(res, err);
    }
    return httpResponse.successResponse(res, users);
  });

};


exports.editMyProfile = function(req, res) {
  var whereQuery = {},
    updateQuery = {},
    options = {};
  whereQuery['_id'] = req.user._id;

  console.log(req.user._id);

  User.findById(req.user._id, function(err, userDocument) {
    if (!userDocument) return handleError(res, 'No User found');
    var updated = _.merge(userDocument, req.body);

    var editedProfile = new User(updated, {
      _id: false
    });
    editedProfile.save(function(errSave, data) {
      if (errSave) {
        return handleError(res, errSave);
      }
      return httpResponse.successResponse(res, data);
    })

  })

};


exports.editProfile = function(req, res) {
  var whereQuery = {},
    updateQuery = {},
    options = {};
  whereQuery['_id'] = req.body._id;
  console.log(whereQuery);

  User.findById(whereQuery, function(err, userDocument) {
    if (!userDocument) return handleError(res, 'No User found');
    var updated = _.merge(userDocument, req.body);

    var editedProfile = new User(updated, {
      _id: false
    });
    editedProfile.save(function(errSave, data) {
      if (errSave) {
        return handleError(res, errSave);
      }
      return httpResponse.successResponse(res, data);
    })
  })
};


exports.editMyBank = function(req, res) {
  var whereQuery = {},
    updateQuery = {},
    options = {};
  whereQuery['_id'] = req.params.user_id;
  User.findById(req.params.user_id, function(err, userDocument) {
    if (!userDocument) return handleError(res, 'No User found');


    var subject = 'User id :' + userDocument._id + '\n' + 'User name :' + userDocument.name;

    Email.bankUpdate(subject);

    Success.successResponse(res, {
      message: 'done'
    }, 200);


  })
};


exports.getUsers = function(req, res) {
  var whereQuery = {},
    selectedOptions = {},
    limit = 250,
    page = 0,
    sortBy, skip;

  if (req.params.user_id) whereQuery['_id'] = req.params.user_id;
  if (req.params.user_email) whereQuery['email'] = req.params.user_email;
  if (req.params.limit) limit = req.params.limit;
  if (req.params.page) page = req.params.page;


  findUsersUtil(whereQuery, selectedOptions, limit, sortBy, skip, function(err, users) {
    if (err) {
      return handleError(res, err);
    }
    return httpResponse.successResponse(res, users);
  });
};


exports.getAllUsers = function(req, res) {
  var whereQuery = {},
    selectedOptions = {},
    limit = 250,
    page = 0,
    sortBy, skip;

  if (req.params.limit) limit = req.params.limit;


  findUsersUtil(whereQuery, selectedOptions, limit, sortBy, skip, function(err, users) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong here.",
        errorInfo: err
      });
    }
    return res.status(200).send({
      status: "success",
      response: users
    });
  });
};


exports.getVideos = function(req, res) {
  var dummyObject = [{
    'url': 'https://youtu.be/eH4MB2x2W3k',
    'text': 'How Muffin works',
    'image': 'https://s3-ap-southeast-1.amazonaws.com/muffvidimages/Video1.png'
  }, {
    'url': 'https://youtu.be/UTee9qmsjCM',
    'text': 'Why use Muffin?',
    'image': 'https://s3-ap-southeast-1.amazonaws.com/muffvidimages/Video2.png'
  }];
  var finalObject = {};

  finalObject.results = dummyObject;
  return httpResponse.successResponse(res, finalObject);

};


exports.updateDevice = function(req, res) {
  var user_id = req.body.user_id;
  var device = req.body.device;
  var name = device.name;


  Devices.update({
    'device.android_id': device.android_id
  }, req.body, {
    upsert: true,
    setDefaultsOnInsert: true
  }, function(err, success) {
    console.log(device);
    if (success) {
      Success.successResponse(res, {
        message: 'done'
      }, 200);
    } else {
      Success.errorResponse(res, 'Unable to update device', 500, 'Unable to update device');
    }


  });


};
exports.amountDue = function(req, res) {

  var userId = req.body.userId;
  var amount = req.body.amount;

  User.findByIdAndUpdate({
    _id: userId
  }, {
    $inc: {
      amountDue: amount
    }
  }, function(err, data) {

    Success.successResponse(res, {
      message: 'done'
    }, 200);

  });

};

exports.isKycCaDone = function(req, res) {
  User.find({}).where('kycDone').equals('true')
    .where('creditAssessmentDone').equals('true')
    .exec(function(err, data) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      res.status(200).send({
        status: "success",
        response: data
      });
    });
}


exports.index = function (req, res) {
  User.find(function (err, users) {

    if (err) {
      res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
    }
    
    res.status(200).send({
      status: "success",
      response: users
    });
  });
};