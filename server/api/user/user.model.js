'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var authTypes = ['github', 'twitter', 'facebook', 'google'];

var UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    lowercase: true
  },
  email_work: {
    type: String,
    lowercase: true
  },

  password: String,
  pin: String,
  fbId: String,
  fbToken: String,
  is_phone_verified: Boolean,
  gender: String,

  phone: String,
  phone_work: String,
  phone_res: String,

  address: String,
  pincode: String,

  address_work: String,
  pincode_work: String,

  panCard: String,
  aadharCard: String,
  creditLimit: Number,
  signUpSource: {
    type: String,
    default: 'muffinWeb'
  },
  link: String,
  isDefaulter: {
    type: Boolean,
    default: false
  },
  creditAssessmentDone: {
    type: Boolean,
    default: false
  },
  kycDone: {
    type: Boolean,
    default: false
  },

  amountDue: Number,
  fathersName: String,
  mothersName: String,
  maritalStatus: Boolean,
  spouseName: String,
  dob: String,
  anniversary: String,

  userBanks: {

    active: Boolean,
    bankName: String,
    bankAcNumber: String,
    bankBranch: String,
    bankIfsc: String,

  },



  pic: {

    userPic: String,
    coverPic: String,
    userPicThumb: String

  },


  doc: {},

  occupations: [{

    currentEmployer: String,
    employedSince: String,
    totalExp: String,
    postion: String,
    annualSalary: Number
  }],

  verification: {

    isVerified: Boolean,

    father: {

      name: String,
      address: String,
      phone: String,
      email: String,
      pan: String,
      relation: String,
      dob: String

    },

    spouse: {

      name: String,
      address: String,
      phone: String,
      email: String,
      pan: String,
      relation: String,
      dob: String

    },

    nominee: {

      name: String,
      address: String,
      phone: String,
      email: String,
      pan: String,
      relation: String,
      dob: String

    },

    guarantor: {

      name: String,
      address: String,
      phone: String,
      email: String,
      pan: String,
      relation: String,
      dob: String

    }

  },

  role: {
    type: String,
    default: 'user'
  },
  provider: String,
  salt: String,
  facebook: {},
  twitter: {},
  google: {},
  github: {}
});

/**
 * Virtuals
 */
UserSchema
  .virtual('password_dummy')
  .set(function (password_dummy) {
    this._password = password_dummy;
    this.salt = this.makeSalt();
    this.password = this.encryptPassword(password_dummy);
  })
  .get(function () {
    return this._password;
  });

// Public profile information
UserSchema
  .virtual('profile')
  .get(function () {
    return {
      'name': this.name,
      'role': this.role
    };
  });

// Non-sensitive info we'll be putting in the token
UserSchema
  .virtual('token')
  .get(function () {
    return {
      '_id': this._id,
      'role': this.role
    };
  });

/**
 * Validations
 */

// Validate empty email
UserSchema
  .path('email')
  .validate(function (email) {
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return email.length;
  }, 'Email cannot be blank');

// Validate empty password
UserSchema
  .path('password')
  .validate(function (password) {
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return password.length;
  }, 'Password cannot be blank');

// Validate email is not taken
UserSchema
  .path('email')
  .validate(function (value, respond) {
    var self = this;
    this.constructor.findOne({
      email: value
    }, function (err, user) {
      if (err) throw err;
      if (user) {
        if (self.id === user.id) return respond(true);
        return respond(false);
      }
      respond(true);
    });
  }, 'The specified email address is already in use.');

UserSchema
  .path('username')
  .validate(function (value, respond) {
    var self = this;
    this.constructor.findOne({
      username: value
    }, function (err, user) {
      if (err) throw err;
      if (user) {
        if (self.id === user.id) return respond(true);
        return respond(false);
      }
      respond(true);
    });
  }, 'The specified username has already been taken.');

var validatePresenceOf = function (value) {
  return value && value.length;
};

/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */
  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.password;
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */
  makeSalt: function () {
    return crypto.randomBytes(16).toString('base64');
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */
  encryptPassword: function (password) {
    if (!password || !this.salt) return '';
    var salt = new Buffer(this.salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  }
};

module.exports = mongoose.model('User', UserSchema);