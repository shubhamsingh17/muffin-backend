'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./reports-installmentpayable.controller');
router.get('/', controller.all);
router.get('/payable', controller.index); // already sorted list of installment payable 
router.get('/:id', controller.show);

router.post('/findbyid', controller.findid);

router.post('/new', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);

router.post('/find', controller.find);
module.exports = router;