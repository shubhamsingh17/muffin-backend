'use strict';

var _ = require('lodash');
var ReportsInstallmentpayable = require('./reports-installmentpayable.model');
var Muffin = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var Auctions = require('../auctions/auctions.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var async = require('async');
var utils = require('../../utils/utilityFunctions.js');
var moment = require('moment');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Installmentpayable
 *
 * @param req
 * @param res
 */



 exports.all = function(req, res) {
  ReportsInstallmentpayable.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};


exports.index = function(req, res) {
  ReportsInstallmentpayable.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    var calInstallment = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i].amountDue != 0 && data[i].amountPaid != 0) {
        calInstallment.push(data[i]);
      }
    }
    res.status(200).send({
      status: "success",
      response: calInstallment
    });

  });
};

/**
 * Get a single Installmentpayable
 *
 * @param req
 * @param res
 */
 exports.show = function(req, res) {
  ReportsInstallmentpayable.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

exports.findid = function(req, res) {
  console.log(req.body);
  console.log("inside reports-installmentpayable api");
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;

  if (!userId) {
    return Errors.errorMissingParam(res, 'userId');
  }
  if (!muffinId) {
    return Errors.errorMissingParam(res, 'muffinId');
  }  

  ReportsInstallmentpayable.find({
    'userId': userId,
    'muffinId': muffinId
  }, function(err, data) {
    if (err) {
      console.log(err);
      return httpResponse.errorResponse(res, err, 422);
    }
    if (!data) {
      return httpResponse.errorResponse(res, err, 422);
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Creates a new Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
 exports.create = function(req, res) {
  ReportsInstallmentpayable.create(req.body, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Updates an existing Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
 exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ReportsInstallmentpayable.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong.",
        errorInfo: err
      })
    }
    var updated = _.merge(data, req.body);
    updated.save(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        response: data
      });
    });
  });
};

/**
 * Deletes a Installmentpayable from the DB.
 *
 * @param req
 * @param res
 */
 exports.destroy = function(req, res) {
  ReportsInstallmentpayable.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong.",
        errorInfo: err
      })
    }
    ReportsInstallmentpayable.remove(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        message: "deleted successfully"
      });
    });
  });
};
// installment payable = 5% of faceValue / total no of members + highestBid/total no of members

exports.find = function(req, res) {
  var auctionId = req.body.auctionId;
  Auctions.findById(auctionId)
  .populate('muffinId')
  .exec(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    var instCal = [];
    var faceValue = data.members * data.instalment;
    var allInst = Math.floor((.05 * faceValue) / 24 + (data.highestBid.bid / 24));
    instCal.push({installment: allInst});
    res.status(200).send({
      "res": instCal
    });
    
  });
}