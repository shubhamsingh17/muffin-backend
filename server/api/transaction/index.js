'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./transaction.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/getTransactions/:userId/:muffinId', controller.getTransactions);

router.post('/', controller.create);
router.post('/addTransaction', controller.addTransaction);

router.put('/:id', controller.update);

router.delete('/:id', controller.destroy);

module.exports = router;
