'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./auctions.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/getCurrentAuction/:muffin_id', controller.getCurrentAuction);

router.post('/', controller.create);

router.put('/:id', controller.update);
router.put('/finishAuctionCall', controller.finishAuctionCall);
router.get('/getAuctions/:muffinId', controller.getAuctions);
router.get('/getAuction/:auctionId', controller.getAuction);
router.get('/startAuctionCall/:muffinId', controller.startAuctionCall);

router.delete('/:id', controller.destroy);

module.exports = router;