'use strict';

var _ = require('lodash');
var Auctions = require('./auctions.model');
var Statements = require('../statements/statements.model');
var MuffinForUsers = require('../muffins-for-users/muffins-for-users.model');
var Muffin = require('../muffin/muffin.model');
var User = require('../user/user.model');
var httpResponse = require('../../responses');
var schedule = require('node-schedule');
var crontab = require('node-crontab');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Auctions 1
 *
 * @param req
 * @param res
 */
exports.index = function (req, res) {
  Auctions.find(function (err, auctionss) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
			status: "success",
			response: auctionss
		});
  });
};

/**
 * Get a single Auctions
 *
 * @param req
 * @param res
 */
exports.show = function (req, res) {
  Auctions.findById(req.params.id, function (err, auctions) {
    if (err) {
      res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
    }
    if (!auctions) {
      return res.status(404).end();
    }
    return res.status(200).send({
			status: "success",
			response: auctions
		});
  });
};

/**
 * Creates a new Auctions in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function (req, res) {
  Auctions.create(req.body, function (err, auctions) {
    if (err) {
      res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
    }
    return res.status(201).json(auctions);
  });
};

/**
 * Updates an existing Auctions in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Auctions.findById(req.params.id, function (err, auctions) {
    if (err) {
      res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
    }
    if (!auctions) {
      return res.status(404).end();
    }
    var updated = _.merge(auctions, req.body);
    updated.save(function (err) {
      if (err) {
        res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
      }
      return res.status(200).send({
			status: "success",
			response: auctions
		});
    });
  });
};


exports.getAuctions = function (req, res) {

  var muffinId = req.params.muffinId;

  Auctions.find({"muffinId": muffinId, "auctionDone": true}, function (err, auctions) {

    return httpResponse.successResponse(res, {auctions:auctions});
  }).select("-bids").populate("highestBid.user_id", "name pic").exec();
};


exports.startAuctionCall = function (req, res) {

  console.log(req.params.muffinId);

  if (req.params.muffinId) {

    startAuction(req.params.muffinId, res);

  }
  return httpResponse.successResponse(res, "Success");    

};


exports.finishAuctionCall = function (req, res) {

  auctionFinish(req.body.auction_id, res);

};


exports.getCurrentAuction = function (req, res) {

  var muffinId = req.params.muffin_id;

  Muffin.findById(muffinId, function (err, data) {

    if (data) {

      console.log(data.currentBidding);

      var currentAuctionNumber = data.currentBidding;



      Auctions.findOne({'muffinId': muffinId, 'auctionNumber': currentAuctionNumber}, function (err, data) {
        console.log(data);
        return httpResponse.successResponse(res, data);
      }).populate("highestBid.user_id bids.user_id", "name pic").exec();

    }

  })

};


exports.getAuction = function (req, res) {

  var auctionId = req.params.auctionId;

  Auctions.findById(auctionId, function (err, data) {

    return httpResponse.successResponse(res, data);

  }).populate("highestBid.user_id bids.user_id", "name pic").exec();


};


/**
 * Deletes a Auctions from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function (req, res) {
  Auctions.findById(req.params.id, function (err, auctions) {
    if (err) {
      res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
    }
    if (!auctions) {
      return res.status(404).end();
    }
    auctions.remove(function (err) {
      if (err) {
        res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
      }
      return res.status(204).end();
    });
  });
};


function auctionFinish(auctionId, res) {

  var muffinId, winUserId, highestBid;

  Auctions.findById(auctionId, function (err, auctionsData) {

    auctionsData.auctionDone = true;

    auctionsData.save(function (err, data) {
      console.log(err);
      console.log("      ----- data -----  ")
      console.log(data);
    });

    muffinId = auctionsData.muffinId;

    console.log("auctions "+auctionsData);

    winUserId = auctionsData.highestBid.user_id;
    highestBid = auctionsData.highestBid.bid;

    var receivable;
    var payable;


    Muffin.findById(muffinId, function (err, data) {

      console.log("Muffin "+data);

      if (data) {
        receivable = highestBid;

        payable = ((highestBid + ((5 * data.chitValue) / 100)) / data.members);

        var currentAuctionNumber = data.currentAuctionNumber;

        var updateNumber = currentAuctionNumber + 1;

        data.currentAuctionNumber = updateNumber;

        data.state = 'active';

        data.save(function (err, data) {

        });

        var winUserObj = {
          "muffin_id": muffinId,
          "auction_id": auctionId,
          "user_id": winUserId,

          "amountReceivable": receivable,
          "amountPayable": payable
        };

        Statements.create(winUserObj, function (err, data) {

          console.log(data);

        });

        var totalPayable = receivable - payable;

        User.findByIdAndUpdate({_id: winUserId}, {$inc: {amountDue: totalPayable}}, function (err, UserData) {


          console.log("User "+UserData);

        });


        MuffinForUsers.find({"muffin_id": muffinId}, function (err, MuffinForUsersData) {

          console.log("MuffinForUsers "+data);

          for (var i = 0; i < MuffinForUsersData.length; i++) {

            var singleUser = MuffinForUsersData[i];

            if (singleUser.userId != winUserId) {

              var normalUserObj = {
                "muffin_id": muffinId,
                "auction_id": auctionId,
                "user_id": singleUser.userId,

                "amountPayable": payable
              };


              Statements.create(normalUserObj, function (err, Statementsdata) {

                console.log(Statementsdata);

              });

              User.findByIdAndUpdate({_id: singleUser.userId}, {$inc: {amountDue: payable}}, function (err, UserData) {


                console.log("User "+UserData);

              });

            }


          }


        });

      }

    });

    return httpResponse.successResponse(res, auctionsData);

  });


}


function startAuction(muffinId, res) {

  var currentAuctionNumber;
  
  var userIds = [];
  // MuffinForUsers.find({"muffin_id": muffinId}, function (err, data) {

  //   if (data) {
  //     userIds.push(data.userId);

  //     currentAuctionNumber = data.currentBidding;
  //   }

  // });
  // var auctionStartDate;

  // Auctions.findOne({'muffinId': muffinId, 'auctionNumber': currentAuctionNumber}, function (err, data) {

  //       console.log(data);

  Muffin.findById(muffinId, function (err, data) {

    if (data) {

      console.log(data.currentBidding);

      var currentAuctionNumber = data.currentBidding;
      var auctionDone = false;

      Auctions.findOne({'muffinId': muffinId, 'auctionNumber': currentAuctionNumber, 'auctionDone': auctionDone}, function (err, data) {
        console.log(data);
        var auctionStartDate;
        if (data && data.auctionDate) {
          auctionStartDate = data.auctionDate;
        }

      

  if (auctionStartDate){
    //schedule a cron job which starts the timer
    var dayOfWeek, month, dayOfMonth, hour, minute, second;

    auctionStartDate.setMinutes(auctionStartDate.getMinutes() - 330);
    month = auctionStartDate.getUTCMonth() + 1;
    dayOfMonth = auctionStartDate.getUTCDate();
    hour = auctionStartDate.getUTCHours();
    minute = auctionStartDate.getUTCMinutes();
    second = auctionStartDate.getUTCSeconds();
    dayOfWeek = auctionStartDate.getDay();

    var cronExpression = second + " " + minute + " " + hour + " " + dayOfMonth + " " + month + " " + dayOfWeek;
    console.log("Cron Expression for " + data._id + "is = " + cronExpression);
    console.log('THE AUCTION HAS BEEN SCHEDULED FOR = ' + auctionStartDate);

    var j = schedule.scheduleJob(cronExpression, function(){
       console.log('THE AUCTION IS STARTED AT : ' + auctionStartDate);
       calculateTimeLeft(data._id);
     });

  } else {
    console.log("There is no auction start Date , So Auction cant be scheduled");
  }

});
}
});


  // var whereQuery = {}, updateQuery = {}, options = {};

  // whereQuery['_id'] = muffinId;
  // updateQuery = {'$set': {'state': 'bidding_active'}};

  // updateMuffinsUtil(whereQuery, updateQuery, options, function (err, data) {

  //   return httpResponse.successResponse(res, data);

  // });

  // sendPushForAuction(userIds);


}


function sendPushForAuction(userIds) {


}

function calculateTimeLeft(auctionId) {
  console.log("inside");
  Auctions.findOne({'_id': auctionId}, function (err, data) {

    console.log(data);
    if (data) {
      var timeLeft , timeLeftn;
      var whereQuery = {}, updateQuery = {}, options = {};

      whereQuery['_id'] = auctionId;

      var cronExpression = "*/1 * * * * *";
      var i = crontab.scheduleJob(cronExpression, function(){

        Auctions.findOne({'_id': auctionId}, function (err, dataInside) {
        
          if (dataInside && dataInside.timeLeft) {
            timeLeftn = dataInside.timeLeft;
          } else {
            timeLeftn = 0;
          }
          
          console.log("timeLeft for " + auctionId + " is " + timeLeftn);

          if(timeLeftn <= 0) {
            console.log("Start");

            // var updateQueryFinal = {'$set': {'auctionDone': true}};
            // Auctions.findOneAndUpdate(whereQuery, updateQueryFinal, options, function (err, object){
            //   console.log("error");
            //   console.log(err);
            //  });
            crontab.cancelJob(i);
            afterAuction(auctionId);
            console.log("end");
          } else {
            timeLeft = timeLeftn - 1;
            updateQuery = {'$set': {'timeLeft': timeLeft}};
            Auctions.findOneAndUpdate(whereQuery, updateQuery, options, function (err, object){
              console.log("Updated error and timeLeft");
              console.log(err);
              console.log(object.timeLeft, timeLeft);
          });
          }
        });
      });
    }
  });
}

function afterAuction(auctionId) {

  var muffinId, winUserId, highestBid;

  Auctions.findById(auctionId, function (err, auctionsData) {

    auctionsData.auctionDone = true;

    auctionsData.save(function (err, data) {
      console.log(err);
      console.log("      ----- data -----  ")
      console.log(data);
    });

    muffinId = auctionsData.muffinId;

    console.log("auctions "+auctionsData);

    winUserId = auctionsData.highestBid.user_id;
    highestBid = auctionsData.highestBid.bid;

    var receivable;
    var payable;


    Muffin.findById(muffinId, function (err, data) {

      console.log("Muffin "+data);

      if (data) {
        receivable = highestBid;

        payable = ((highestBid + ((5 * data.chitValue) / 100)) / data.members);

        var currentAuctionNumber = data.currentBidding;

        var updateNumber = currentAuctionNumber + 1;

        data.currentBidding = updateNumber;

        data.state = 'active';

        data.save(function (err, data) {

        });

        var winUserObj = {
          "muffin_id": muffinId,
          "auction_id": auctionId,
          "user_id": winUserId,

          "amountReceivable": receivable,
          "amountPayable": payable
        };

        Statements.create(winUserObj, function (err, data) {

          console.log(data);

        });

        var totalPayable = payable - receivable;

        User.findByIdAndUpdate({_id: winUserId}, {$inc: {amountDue: totalPayable}}, function (err, UserData) {


          console.log("User "+UserData);

        });


        MuffinForUsers.find({"muffinId": muffinId}, function (err, MuffinForUsersData) {

          console.log("MuffinForUsers "+data);

          for (var i = 0; i < MuffinForUsersData.length; i++) {

            console.log("winner user id " + winUserId);
            var singleUser = MuffinForUsersData[i];

            if (singleUser.userId != winUserId) {

              var normalUserObj = {
                "muffin_id": muffinId,
                "auction_id": auctionId,
                "user_id": singleUser.userId,

                "amountPayable": payable
              };


              Statements.create(normalUserObj, function (err, Statementsdata) {

                console.log(Statementsdata);

              });

              User.findByIdAndUpdate({_id: singleUser.userId}, {$inc: {amountDue: payable}}, function (err, UserData) {


                console.log("User "+UserData);

              });

            }


          }


        });

      }

    });
  });


}

function updateMuffinsUtil(whereQuery, data, options, callback) {

  console.log(whereQuery);

  Muffin.findOneAndUpdate(whereQuery, data, options, function (err, object) {

    console.log(err, object);
    if (err) callback(err, false)
    if (object == null)  return callback('No Data found', false);
    callback(null, object);
  });
}
