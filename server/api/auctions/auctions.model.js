'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AuctionsSchema = new Schema({
  muffinId: {type: mongoose.Schema.Types.ObjectId, ref: 'Muffin'},
  muffinName: {type: String},
  auctionDate: Date,
  auctionDone: Boolean,
  auctionNumber: Number,
  highestBid: {
    user_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    bid: Number
  },
  bids: [{
    bid: Number,
    time: {type: Date, default: Date.now()},
    user_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
  }],
  timeLeft: Number  

});

module.exports = mongoose.model('Auctions', AuctionsSchema);
