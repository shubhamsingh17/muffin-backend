'use strict';
// Setup basic express server
// var express = require('express');
// var app = express();
// var server = require('http').createServer(app);
// var socket = require('socket.io')(server, {
//   serveClient: true
// });
// var port = process.env.PORT || 5000;

// var Auctions = require('./auctions.model');


// var config = require('../../config/environment');
// var mongoose = require('mongoose');

// mongoose.connect(config.mongo.uri, config.mongo.options);

// server.listen(port, function () {
//   console.log('Server listening at port dzvbdb %d', port);
// });

// // Routing
// app.use(express.static(__dirname + '../../../../static_files/public'));

// // Chatroompublic

// var numUsers = 0;

var Auctions = require('./auctions.model');


//var schedule = require('node-schedule');




exports.register = function (socket) {
  var addedUser = false;

  // Auctions.find({"auctionDone": false}, function (err, auctions) {

  //     for (var i = 0; i < auctions.length; i++) {
  //       var j = schedule.scheduleJob(auctions[i].auctionDate, function(){
  //         console.log('The world is going to end today.');
  //         socket.broadcast.emit()
  //     });
  //     }
    
  // });




  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    console.log(data);
    newBid(data.auctionId, data.userId, data.bid, function (data) {

      socket.broadcast.emit('bid_update');
    });
  });



  socket.on('refresh', function (data) {
    // we tell the client to execute 'new message'

    // newBid(data.auctionId, data.userId, data.bid, function(data){

    console.log(data);


    socket.broadcast.emit('refresh', {});

    // });


  });


  // when the client emits 'add user', this listens and executes
  socket.on('add user', function (username) {
    if (addedUser) return;

    // we store the username in the socket session for this client
    // socket.username = username;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });
    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    if (addedUser) {
      --numUsers;
      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });
};


function newBid(auctionId, userId, bid, callback) {

  var newUser = {
    "bid": bid,
    "user_id": userId,
  }
  var whereQuery = {}, updateQuery = {}, options = {};

  whereQuery['_id'] = auctionId;

  // console.log(newUser);

  Auctions.findById(auctionId, function (err, data) {

    if (data && data.auctionDone != true) {

      data.bids.unshift(newUser);

      console.log(err, data + "  bid  " + bid);

      if (!data.highestBid.bid) {
        data.highestBid.bid = 1000000
      }

      if (bid < data.highestBid.bid) {

        data.highestBid.bid = bid;
        data.highestBid.user_id = userId;

      

        if(data.timeLeft <= 30) {
          data.timeLeft = data.timeLeft + 60;
          console.log("Time Updated as the last bid was within 30 seconds : " + data.timeLeft);
        }

        console.log("new bid " + data);

        data.save(function (err, updatedData) {

          console.log("err  " + err + "  updated " + updatedData);

        });

      }

    }

    console.log("callback");
    callback(data);

  });

}


function updateMuffinsUtil(whereQuery, data, options, callback) {
  Auctions.findOneAndUpdate(whereQuery, data, options, function (err, object) {
    if (err) callback(err, false)
    if (object == null)  return callback('No Data found', false);
    callback(null, true);
  });
}