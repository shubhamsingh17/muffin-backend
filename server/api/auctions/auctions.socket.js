'use strict';

var Auctions = require('./auctions.model');

exports.register = function (socket) {

  Auctions.schema.post('save', function (doc) {
    socket.emit('Auctions:save', doc);
  });

  Auctions.schema.post('remove', function (doc) {
    socket.emit('Auctions:remove', doc);
  });

};
