'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./reports-bouncecharges.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/new', controller.create);
router.post('/findbyid', controller.findid);

router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.post('/cal', controller.checkTransction);
module.exports = router;