'use strict';

var _ = require('lodash');
var ReportsBouncecharges = require('./reports-bouncecharges.model');
var Muffins = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var Auctions = require('../auctions/auctions.model');
var Reports = require('../reports/report.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var async = require('async');
var utils = require('../../utils/utilityFunctions.js');
var moment = require('moment');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Installmentpayable
 *
 * @param req
 * @param res
 */


exports.index = function(req, res) {
  ReportsBouncecharges.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Get a single Installmentpayable
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  ReportsBouncecharges.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Creates a new Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
  var muffinId = req.body.muffinId;
  var userId = req.body.userId;
  if (!req.body.muffinId) return Error.errorMissingParam(res, 'muffinId');
  if (!req.body.userId) return Error.errorMissingParam(res, 'userId');
  ReportsBouncecharges.create(req.body, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Updates an existing Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ReportsBouncecharges.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    var updated = _.merge(data, req.body);
    updated.save(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        response: data
      });
    });
  });
};

exports.findid = function(req, res) {
  console.log(req.body);
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;

  if (!userId) {
    return Errors.errorMissingParam(res, 'userId');
  }
  if (!muffinId) {
    return Errors.errorMissingParam(res, 'muffinId');
  }  

  ReportsBouncecharges.find({
    'userId': userId,
    'muffinId': muffinId
  }, function(err, data) {
    if (err) {
      console.log(err);
      return httpResponse.errorResponse(res, err, 422);
    }
    if (!data) {
      return httpResponse.errorResponse(res, err, 422);
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Deletes a Installmentpayable from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  ReportsBouncecharges.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    ReportsBouncecharges.remove(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(204).end();
    });
  });
};


exports.checkTransction = function(req, res) {
  var reportId = req.body.reportId;


  Reports.findById(reportId)
    .populate('reportId')
    .exec(function(err, data) {

      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      } else if (data) {
        console.log(data);
        return false;
        var outObject = {};

      }
      var instCal = [];
      var faceValue = 10000;
      var allInst = Math.floor((.05 * faceValue) / 24 + (data.highestBid.bid / 24));
      instCal.push({
        installment: allInst
      });
      res.status(200).send({
        "res": instCal
      });

    });
}

exports.getMyAmountBal = function(req, res) {
  var userId = req.body.userId;
  if (userId) {
    ReportsAmountpaid.findOne({
      userId: userId
    }, function(err, amount) {
      if (err) {
        res.status(200).send({
          error: ErrorParser.parseErrorreturnErrorMessage(err)
        });
      } else if (amount) {

        var outObject = {};
        outObject.amountDue = amount.amountDue;
        outObject.amountPaid = amount.amountPaid;
        outObject.amountBalance = Math.floor(outObject.amountDue - outObject.amountPaid);
        outObject.auctionId = amount.auctionId;
        outObject.auctionNum = amount.auctionNum;
        outObject.muffinId = amount.muffinId;
        outObject.userId = amount.userId;
        outObject._id = amount._id;
        res.status(200).send({
          status: "success",
          res: outObject
        });
      } else {
        res.status(200).send({
          error: 'No Data Found'
        });
      }
    });
  } else {
    res.status(200).send({
      error: 'No User Id Provided'
    });
  }
};