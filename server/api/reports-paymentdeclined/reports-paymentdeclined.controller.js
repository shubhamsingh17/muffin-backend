'use strict';

var _ = require('lodash');
var ReportsPaymentdeclined = require('./reports-paymentdeclined.model');
var Muffins = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var Auctions = require('../auctions/auctions.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var async = require('async');
var utils = require('../../utils/utilityFunctions.js');
var moment = require('moment');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Installmentpayable
 *
 * @param req
 * @param res
 */


exports.index = function(req, res) {
  ReportsPaymentdeclined.find(function(err, data) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(data);
  });
};

/**
 * Get a single Installmentpayable
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  ReportsPaymentdeclined.findById(req.params.id, function(err, data) {
    if (err) {
      return handleError(res, err);
    }
    if (!data) {
      return res.status(404).end();
    }
    return res.status(200).json(data);
  });
};

/**
 * Creates a new Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
  ReportsPaymentdeclined.create(req.body, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Updates an existing Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ReportsPaymentdeclined.findById(req.params.id, function(err, data) {
    if (err) {
      return handleError(res, err);
    }
    if (!data) {
      return res.status(404).end();
    }
    var updated = _.merge(data, req.body);
    updated.save(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(data);
    });
  });
};

exports.findid = function(req, res) {
  console.log(req.body);
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;

  if (!userId) {
    return Errors.errorMissingParam(res, 'userId');
  }
  if (!muffinId) {
    return Errors.errorMissingParam(res, 'muffinId');
  }  

  ReportsPaymentdeclined.find({
    'userId': userId,
    'muffinId': muffinId
  }, function(err, data) {
    if (err) {
      console.log(err);
      return httpResponse.errorResponse(res, err, 422);
    }
    if (!data) {
      return httpResponse.errorResponse(res, err, 422);
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Deletes a Installmentpayable from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  ReportsPaymentdeclined.findById(req.params.id, function(err, data) {
    if (err) {
      return handleError(res, err);
    }
    if (!data) {
      return res.status(404).end();
    }
    ReportsPaymentdeclined.remove(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).end();
    });
  });
};



exports.getInstallmentpayable = function(req, res) {

  var userId = req.params.userId;

  var installmentpayable = [];

  ReportsPaymentdeclined.find({
    'userId': userId
  }, function(error, muffinIds) {
    if (error) {
      console.log(error);
      return httpResponse.errorResponse(res, error, 500);
    } else {
      var includingIDArray = _.map(muffinIds, function(m) {
        return m && m.state && m.state == 'active' && m.IsMuffinAgreementSigned == true;
      });

      Muffins.find({
        "_id": {
          $in: _.map(includingIDArray, 'muffinId')
        }
      }, function(error, muffinResult) {
        if (error) {
          console.error('error');
          console.error(error);
          return httpResponse.errorResponse(res, error, 500);
        } else {
          console.log(muffinResult);
          var objectToPush = {};
          for (var i = 0; i < muffinResult.length; i++) {
            objectToPush = {};
            objectToPush.muffinDetails = muffinResult[i].toObject();
            objectToPush.faceValue = muffinResult[i].toObject();
            includingIDArray.forEach(function(value) {
              if (value.muffinId.equals(muffinResult[i]._id)) {
                if (value.muffinAgreementSignedDate) {
                  objectToPush.signedDate = value.muffinAgreementSignedDate;
                }
              }
            });

            muffinAgreements.push(objectToPush);
          }
          return httpResponse.successResponse(res, {
            muffinAgreements: muffinAgreements
          });
        }
      });
    }
  });
}


exports.getAvailableMuffins = function(req, res) {

  var userId = req.params.userId;
  var mainMuffinIds = [];

  var excludedMuffinIds = [];

  var muffins = [];

  MuffinsForUsers.find({
    'userId': userId
  }, function(error, muffinIds) {
    if (error) {
      console.log('error');
      console.log(error);
      return httpResponse.errorResponse(res, error, 500);
    } else {
      var excludingIDArray = _.filter(muffinIds, function(m) {
        return m && m.relationType && m.relationType !== 'pending';
      });
      var indexedMuffins = _.indexBy(_.filter(muffinIds, function(m) {
        return m && m.relationType && m.relationType === 'pending';
      }), 'muffinId');
      Muffins.find({
        "state": "approval_queue",
        "_id": {
          "$nin": _.map(excludingIDArray, 'muffinId')
        }
      }, function(error, muffinResult) {
        if (error) {
          console.error('error');
          console.error(error);
          return httpResponse.errorResponse(res, error, 500);
        } else {
          var userIDArray = _.map(_.filter(muffinResult, function(m) {
            return m && m.createdBy;
          }), 'createdBy');
          Users.find({
            '_id': {
              $in: userIDArray
            }
          }, function(error, userResult) {
            if (error) {
              console.error('error');
              console.error(error);
              return httpResponse.errorResponse(res, error, 500);
            } else {
              for (var i = 0; i < muffinResult.length; i++) {
                var objectToPush = muffinResult[i].toObject();
                objectToPush.createdByObject = _.find(userResult, function(u) {
                  return u && u._id.equals(muffinResult[i].createdBy);
                });
                objectToPush.property = indexedMuffins[objectToPush._id]
                muffins.push(objectToPush);
              }
              return httpResponse.successResponse(res, {
                muffins: muffins
              });
            }
          });
        }

      });
    }
  });
}