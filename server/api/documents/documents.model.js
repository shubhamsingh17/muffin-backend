'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DocumentsSchema = new Schema({
  name: String,
  type: String, //pdf, png etc
  category: String, //Agreements, Receipts, cash flow etc
  muffinId: {type: mongoose.Schema.Types.ObjectId, ref: 'Muffin'},
  uploadedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  url: String,
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date, default: Date.now},
  accessedAt: {type: Date, default: Date.now}


});

module.exports = mongoose.model('Documents', DocumentsSchema);
