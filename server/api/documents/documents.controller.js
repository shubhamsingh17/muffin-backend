'use strict';

var _ = require('lodash');
var AWS = require('aws-sdk');
var bodyParser = require('body-parser');
var Documents = require('./documents.model');
var httpResponse = require('../../responses');
var AWSUtils = require('./aws_utils.js');

function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Documents
 *
 * @param req
 * @param res
 */
exports.index = function(req, res) {
  Documents.find(function(err, documentss) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(documentss);
  });
};

/**
 * Get a single Documents
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  Documents.findById(req.params.id, function(err, documents) {
    if (err) {
      return handleError(res, err);
    }
    if (!documents) {
      return res.status(404).end();
    }
    return res.status(200).json(documents);
  });
};

/**
 * Creates a new Documents in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
  Documents.create(req.body, function(err, documents) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(documents);
  });
};

/**
 * Updates an existing Documents in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Documents.findById(req.params.id, function(err, documents) {
    if (err) {
      return handleError(res, err);
    }
    if (!documents) {
      return res.status(404).end();
    }
    var updated = _.merge(documents, req.body);
    updated.save(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(documents);
    });
  });
};

exports.getDoc = function(req, res) {

  var muffinId = req.params.muffinId;
  var userId = req.params.userId;

  Documents.findOne({
    "muffinId": muffinId,
    "userId": userId
  }, function(err, transactions) {

    return httpResponse.successResponse(res, transactions);
  });
};


exports.getUserDoc = function(req, res) {

  var userId = req.user.id;

  Documents.findOne({
    "userId": userId
  }, function(err, transactions) {

    return httpResponse.successResponse(res, transactions);
  });
};

exports.getAllDoc = function(req, res) {

  Documents.find({}, function(err, transactions) {

    return httpResponse.successResponse(res, transactions);
  });
};


/**
 * Deletes a Documents from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  Documents.findById(req.params.id, function(err, documents) {
    if (err) {
      return handleError(res, err);
    }
    if (!documents) {
      return res.status(404).end();
    }
    documents.remove(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).end();
    });
  });
};


exports.uploadedMuffinDocuments = function(req, res) {
  var bucketName = 'delhi-rock';

  var params = {
    "muffinId": req.body.muffinId,
    "userId": req.body.userId,
    "key": req.body.key
  };
  params.url = "https://" + bucketName + ".s3.amazonaws.com/" + params.key;

  Documents.create(params, function(err, document) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      });
    } else {
      res.status(200).send({
        status: "success",
        res: document
      })

    }

  })
}