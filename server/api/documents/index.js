'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./documents.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.delete('/:id', controller.destroy);

router.post('/create', controller.create);

router.put('/:id', controller.update);
router.get('/getDoc/:userId/:muffinId', controller.getDoc);
router.get('/getUserDoc/:userId', controller.getUserDoc);
router.get('/getalldoc', controller.getAllDoc);
router.post('/uploadedDocs', controller.uploadedMuffinDocuments);

module.exports = router;
