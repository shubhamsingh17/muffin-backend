var AWS = require('aws-sdk');


var buckets = {
	bucketName: 'delhi-rock',
};

exports.generateURL = function(key) {
	if (key) {
		return "https://" + buckets.bucketName + ".s3.amazonaws.com/" + key;
	} else {
		return null
	}
};