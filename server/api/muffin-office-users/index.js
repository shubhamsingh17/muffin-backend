'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./muffin-office-users.controller');
var auth = require('../../auth/auth.service');

router.post('/create', controller.create); // create admin(or sales executive) and superadmin(signup)
router.post('/login', controller.login); // login admin and superadmin
router.post('/loginNew', controller.loginNew);
// forgot password admin and superadmin (superadmin can retrieve admin password)
// logout admin and superadmin (chitra will handle after destroy token)
router.get('/', controller.index);
router.post('/', controller.myDashboard);
module.exports = router;