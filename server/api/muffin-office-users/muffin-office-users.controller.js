'use strict';

var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var MuffinOfficeUsers = require('./muffin-office-users.model');
var _ = require('lodash');

var Errors = require('../../error');
var Success = require('../../responses');
var Email = require('../../utils/emailSender');
var Promise = require('bluebird');
var httpResponse = require('../../responses');
var Error = require('../../error');
var MuffinOfficeUsersObj = Promise.promisifyAll(MuffinOfficeUsers);
var utils = require('../../utils/utilityFunctions.js');
/**
 * Handles error here.
 *
 * @param req
 * @param res
 */

function handleError(res, err) {
  return res.status(500).send(err);
}
/**
 * Creates a new user in the DB.
 *
 * @param req
 * @param res
 */

exports.create = function(req, res) {
  var user = req.body;
  var email = req.body.email;
  var username = req.body.username;
  var phone = req.body.phone;
  var password = req.body.password;
  if (!email) {
    return Errors.errorMissingParam(res, 'email');
  }
  if (!phone) {
    return Errors.errorMissingParam(res, 'phone');
  }
  if (!username) {
    return Errors.errorMissingParam(res, 'username');
  }
  if (!password) {
    return Errors.errorMissingParam(res, 'password');
  } else {
    createUser(user, res);
  }
};

/**
 * function for creating users.
 *
 * @param req
 * @param res
 */

function createUser(user, res) {

  MuffinOfficeUsers.create(user, function(err, users) {

    if (err) {
      res.status(200).send({
        error: 'something went wrong here'
      });
    } else {
      var token = jwt.sign({
          _id: MuffinOfficeUsers._id
        },
        config.secrets.session, {
          expiresInMinutes: 10 * 365 * 24 * 60
        }
      );
      var response = {
        'token': token,
        'users': users
      };

      return Success.successResponse(res, response, 200);

    }
  });
}

/**
 * login using token and password.
 *
 * @param req
 * @param res
 */

exports.login = function(req, res) {
  var email = req.body.email;
  var password = req.body.password;
  var role = req.body.role;

  if (!email) {
    return Errors.errorMissingParam(res, 'email');
  }
  if (!password) {
    return Errors.errorMissingParam(res, 'password');
  }
  if (!role) {
    return Errors.errorMissingParam(res, 'role');
  }

  checkFindEmailPass({
      email: email,
      password: password
    })
    .then(function(MuffinOfficeUsers) {
      if (!MuffinOfficeUsers) {
        Success.errorResponse(res, 'Email or Password not match', 200, 'Email or Password not match');


      } else {
        var token = jwt.sign({
            _id: MuffinOfficeUsers._id
          },
          config.secrets.session, {
            expiresInMinutes: 10 * 365 * 24 * 60
          }
        );

        var response = {
          'token': token,
          'MuffinOfficeUsers': MuffinOfficeUsers
        };
        return Success.successResponse(res,
          response, 200);


      }
    });

};


exports.loginNew = function(req, res) {
  var email = req.body.email;
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  var password = req.body.password;
  var paramCheck = utils.hasSufficientParams(req.body, ['email'], [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    if (!token && !password) {
      return Errors.errorMissingParam(res, 'password');
    } else {
      checkFind(email)
        .then(function(MuffinOfficeUsers) {

          if (!MuffinOfficeUsers) {

            Success.errorResponse(res, 'User not present', 401, 'User not present');

          } else {

            var token = jwt.sign({
                _id: user._id
              },
              config.secrets.session, {
                expiresInMinutes: 10 * 365 * 24 * 60
              }
            );



            var response = {
              'token': token,
              'MuffinOfficeUsers': MuffinOfficeUsers
            };
            return Success.successResponse(res,
              response, 200);


          }
        });
    }
  }

};

/**
 * login using token username and phone.
 *
 * @param req
 * @param res
 */

exports.loginUserName = function(req, res) {
  var phone = req.body.phone;
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  var username = req.body.username;


  if (!phone) {
    return Errors.errorMissingParam(res, 'phone');
  }

  if (!username) {
    return Errors.errorMissingParam(res, 'username');
  }
  if (!token) {
    return Errors.errorMissingParam(res, 'token');
  }

  checkFindUserName(username)
    .then(function(MuffinOfficeUsers, err) {
      if (!MuffinOfficeUsers) {

        Success.errorResponse(res, "something went wrong hare!!!");

      } else {

        var token = jwt.sign({
            _id: MuffinOfficeUsers._id
          },
          config.secrets.session, {
            expiresInMinutes: 10 * 365 * 24 * 60
          }
        );

        var response = {
          'token': token,
          'MuffinOfficeUsers': MuffinOfficeUsers
        };
        return Success.successResponse(res,
          response, 200);


      }
    });

};

/**
 * checking email in muffinofficeusers.
 *
 * @param req
 * @param res
 */

function checkFind(email) {
  return MuffinOfficeUsers.findOne({
    'email': email
  });
}

/**
 * checking password in muffinofficeusers.
 *
 * @param req
 * @param res
 */

function checkFindPassword(password) {
  return MuffinOfficeUsers.findOne({
    'password': password
  });
}

/**
 * checking username in muffinofficeusers.
 *
 * @param req
 * @param res
 */

function checkFindUserName(username) {
  console.log("checkFind " + username);
  return MuffinOfficeUsers.findOne({
    'username': username
  });
}



exports.index = function(req, res) {

  MuffinOfficeUsers.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong while finding users. Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });

};

exports.myDashboard = function(req, res, next) {
  try {
    if (req.MuffinOfficeUsers.role == "superadmin") {
      MuffinOfficeUsers.findOne({
        _id: req.MuffinOfficeUsers._id
      }).exec(function(err, prof) {
        if (err) {
          res.status(200).send({
            status: "error",
            message: err.message
          });
        } else {
          res.status(200).send({
            status: "success",
            response: prof
          });
        }
      });
    } else {
      res.status(200).send({
        status: "error",
        message: "You are not authorized"
      });
    }
  } catch (err) {
    res.status(500).send({
      status: "error",
      message: "Something went wrong while getting users. Please try again."
    });
  }
};

function checkFindEmailPass(data) {
  return MuffinOfficeUsers.findOne(data);
}