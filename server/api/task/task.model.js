'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
  taskId: {
    type: String,
    maxlength: 32,
    unique: true,
    required: true
  },
  taskType: {
    type: String,
    enum: ['legal', 'sales']
  },
  taskDate: {
    type: Date
  },
  taskDetails: {
    type: String
  },
  taskSubject: {
    type: String
  },
  createdBySales: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Sales'
  },

  createdByLegals: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Legals'
  },
});
TaskSchema.set('toObject', { getters: true });
TaskSchema.virtual('formatted_time').get(function() {
  var date = new Date(this.taskDate);
  return (date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear();
});

module.exports = mongoose.model('Task', TaskSchema);