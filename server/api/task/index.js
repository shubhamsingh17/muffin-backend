'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./task.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/create', controller.create);
router.put('/:id', controller.update);
router.get('/findTaskLegals/:createdByLegalsId', controller.findTaskLegals);
router.get('/findTaskSales/:createdBySalesId', controller.findTaskSales);
router.delete('/:id', controller.destroy);
//Search for task Api is remaining.

module.exports = router;