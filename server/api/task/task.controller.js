'use strict';

var _ = require('lodash');
var Task = require('./task.model');
var MuffinOfficeUsers = require('../muffin-office-users/muffin-office-users.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var async = require('async');
var moment = require('moment');
var difference = require('array-difference');
var arrayDiff = require('simple-array-diff');


function handleError(res, err) {
	return res.status(500).send(err);
}

/**
 * Get list of Tasks
 *
 * @param req
 * @param res
 */
exports.index = function(req, res) {
	Task.find(function(err, tasks) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		return res.status(200).send({
			status: "success",
			response: tasks
		});
	});
};

/**
 * Get a single Tasks
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
	Task.findById(req.params.id, function(err, tasks) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		if (!tasks) {
			return res.status(404).send({
				status: "error",
				message: "we are unable to find your request.",
				errorinfo: err
			});
		}
		return res.status(200).send({
			status: "success",
			response: tasks
		});
	});
};

/**
 * Creates a new Task in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
	req.body.taskDate = moment(req.body.taskDate, "DD-MM-YYYY");
	Task.create(req.body, function(err, tasks) {
		if (err) {
			return res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		return res.status(200).send({
			status: "success",
			response: tasks
		});
	});
};

/**
 * Updates an existing Task in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
	if (req.body._id) {
		delete req.body._id;
	}
	Task.findById(req.params.id, function(err, tasks) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		if (!tasks) {
			return res.status(404).send({
				status: "error",
				message: "we are unable to find your request.",
				errorinfo: err
			});
		}
		var updated = _.merge(tasks, req.body);
		updated.save(function(err) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Something went wrong here.",
					errorInfo: err
				});
			}
			return res.status(200).send({
				status: "success",
				response: tasks
			});
		});
	});
};

/**
 * Deletes a Task from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
	Task.findById(req.params.id, function(err, tasks) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		if (!tasks) {
			return res.status(404).send({
				status: "error",
				message: "we are unable to find your request.",
				errorinfo: err
			});
		}
		tasks.remove(function(err) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Something went wrong here.",
					errorInfo: err
				});
			}
			return res.status(200).send({
				status: "success",
				message: "successfully deleted"
			});
		});
	});
};

/**
 *Task Belongs to MuffinOfficeUsers.
 *
 * @param req
 * @param res
 */

exports.getDoc = function(req, res) {

	var createdByLegals = req.params._id;
	console.log(createdByLegals);
	return false;

	Task.find({
		"createdByLegals": createdByLegals
	}, function(err, tasks) {

		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		if (!tasks) {
			return res.status(404).send({
				status: "error",
				message: "we are unable to find your request.",
				errorinfo: err
			});
		}
		return res.status(200).send({
			status: "success",
			response: tasks
		});
	});
};



exports.findTaskLegals = function(req, res) {

	var whereQuery = {},
		limit = 10,
		page = 0,
		sortBy, skip;

	if (req.params.createdByLegalsId) whereQuery['createdByLegals'] = req.params.createdByLegalsId;
	if (req.params.limit) limit = req.params.limit;
	if (req.params.page) page = req.params.page;


	findTaskUtil(whereQuery, limit, sortBy, skip, function(err, tasks) {
		if (err) {
			return handleError(res, err);
		}
		return httpResponse.successResponse(res, tasks);
	});

};

exports.findTaskSales = function(req, res) {

	var whereQuery = {},
		limit = 10,
		page = 0,
		sortBy, skip;

	if (req.params.createdBySalesId) whereQuery['createdBySales'] = req.params.createdBySalesId;
	if (req.params.limit) limit = req.params.limit;
	if (req.params.page) page = req.params.page;


	findTaskUtil(whereQuery, limit, sortBy, skip, function(err, tasks) {
		if (err) {
			return handleError(res, err);
		}
		return httpResponse.successResponse(res, tasks);
	});

};

function findTaskUtil(whereQuery, limit, sortBy, skip, cb) { //util for findMuffin

	if (!limit) limit = 10;
	if (!sortBy) sortBy = '';
	if (!skip) skip = 0;

	Task.find(whereQuery).limit(limit).sort(sortBy)
		.exec(function(err, tasks) {
			cb(null, tasks)
		});

}
/**
 *Search for Task.
 *
 * @param req
 * @param res
 */