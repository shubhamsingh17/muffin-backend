'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./s3store.controller');

router.get('/sign', controller.sign);

module.exports = router;
