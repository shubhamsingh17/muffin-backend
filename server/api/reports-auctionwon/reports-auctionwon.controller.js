'use strict';

var _ = require('lodash');
var ReportsAuctionwon = require('./reports-auctionwon.model');
var Muffins = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var Auctions = require('../auctions/auctions.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var async = require('async');
var utils = require('../../utils/utilityFunctions.js');
var moment = require('moment');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Installmentpayable
 *
 * @param req
 * @param res
 */


exports.index = function(req, res) {
  ReportsAuctionwon.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Get a single Installmentpayable
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  ReportsAuctionwon.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Creates a new Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
  ReportsAuctionwon.create(req.body, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

exports.findid = function(req, res) {
  console.log(req.body);
  var userId = req.body.userId;
  var muffinId = req.body.muffinId;

  if (!userId) {
    return Errors.errorMissingParam(res, 'userId');
  }
  if (!muffinId) {
    return Errors.errorMissingParam(res, 'muffinId');
  }  

  ReportsAuctionwon.find({
    'userId': userId,
    'muffinId': muffinId
  }, function(err, data) {
    if (err) {
      console.log(err);
      return httpResponse.errorResponse(res, err, 422);
    }
    if (!data) {
      return httpResponse.errorResponse(res, err, 422);
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Updates an existing Installmentpayable in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ReportsAuctionwon.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    var updated = _.merge(data, req.body);
    updated.save(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        response: data
      });
    });
  });
};

/**
 * Deletes a Installmentpayable from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  ReportsAuctionwon.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    ReportsAuctionwon.remove(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        message: "Something went wrong, Please try again."

      })
    });
  });
};

exports.whoWonAuction = function(req, res) {
  var auctionId = req.body.auctionId;
  Auctions.findById(auctionId)
    .populate('muffinId')
    .exec(function(err, data) {

      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })

      }
      var outObject = {};
      outObject.auctionId = data._id;
      outObject.muffinId = data.muffinId;
      outObject.muffinName = data.muffinName;
      outObject.highestBidIndl = data.highestBid.user_id;
      outObject.highestBidAmount = data.highestBid.bid;
      outObject.auctionDate = data.auctionDate;
      res.status(200).send({
        status: "success",
        res: outObject
      })

    })

}