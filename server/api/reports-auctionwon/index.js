'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./reports-auctionwon.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/new', controller.create);
router.post('/findbyid', controller.findid);

router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.post('/auctionWinner', controller.whoWonAuction);
module.exports = router;