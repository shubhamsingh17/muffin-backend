  'use strict';

  var _ = require('lodash');
  var MuffinsForUsers = require('./muffins-for-users.model');
  var Muffins = require('../muffin/muffin.model');
  var Users = require('../user/user.model');
  var Auctions = require('../auctions/auctions.model');
  var httpResponse = require('../../responses');
  var Error = require('../../error');
  var Errors = require('../../error');
  var Success = require('../../responses');
  var PUSH = require('../../utils/push-notifications');
  var async = require('async');
  var utils = require('../../utils/utilityFunctions.js');
  var moment = require('moment');

  var difference = require('array-difference');
  var arrayDiff = require('simple-array-diff');


  function handleError(res, err) {
    return res.status(500).send(err);
  }

  /**
   * Get list of MuffinsForUsers
   *
   * @param req
   * @param res
   */


   exports.assignMufffin = function(req, res) {
    var userId = req.body.user_id || "Server"; // can be commented if we have users in DB
    var muffinId = req.body.muffin_id;
    req.body.muffinId = muffinId;
    req.body.userId = userId;
    req.body.relationType = 'subscribed';
    req.body.userId = user_id;
    MuffinsForUsers.create(req.body, function(err, muffindata) {
      if (err) return httpResponse.errorResponse(res, err, 500);

      PUSH.invitePush(user_id, muffinId);

      return httpResponse.successResponse(res, muffindata);

    });

  };

  exports.index = function(req, res) {
    MuffinsForUsers.find(function(err, muffinsForUserss) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(muffinsForUserss);
    });
  };

  /**
   * Get a single MuffinsForUsers
   *
   * @param req
   * @param res
   */
   exports.show = function(req, res) {
    MuffinsForUsers.findById(req.params.id, function(err, muffinsForUsers) {
      if (err) {
        return handleError(res, err);
      }
      if (!muffinsForUsers) {
        return res.status(404).end();
      }
      return res.status(200).json(muffinsForUsers);
    });
  };

  /**
   * Creates a new MuffinsForUsers in the DB.
   *
   * @param req
   * @param res
   */
   exports.create = function(req, res) {
    MuffinsForUsers.create(req.body, function(err, muffinsForUsers) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(201).json(muffinsForUsers);
    });
  };

  /**
   * Updates an existing MuffinsForUsers in the DB.
   *
   * @param req
   * @param res
   */
   exports.update = function(req, res) {
    if (req.body._id) {
      delete req.body._id;
    }
    MuffinsForUsers.findById(req.params.id, function(err, muffinsForUsers) {
      if (err) {
        return handleError(res, err);
      }
      if (!muffinsForUsers) {
        return res.status(404).end();
      }
      var updated = _.merge(muffinsForUsers, req.body);
      updated.save(function(err) {
        if (err) {
          return handleError(res, err);
        }
        return res.status(200).json(muffinsForUsers);
      });
    });
  };

  /**
   * Deletes a MuffinsForUsers from the DB.
   *
   * @param req
   * @param res
   */
   exports.destroy = function(req, res) {
    MuffinsForUsers.findById(req.params.id, function(err, muffinsForUsers) {
      if (err) {
        return handleError(res, err);
      }
      if (!muffinsForUsers) {
        return res.status(404).end();
      }
      muffinsForUsers.remove(function(err) {
        if (err) {
          return handleError(res, err);
        }
        return res.status(204).end();
      });
    });
  };


  exports.testPush = function(req, res) {

    console.log("kebqkbwqkw");

    PUSH.testPush();

  };


  exports.getMyMuffins = function(req, res) {
    var user_id = req.user.id;

    console.log(user_id);

    getMainMuffins(user_id, res);

  };


  exports.getActiveMuffins = function(req, res) {
    var user_id = req.user.id;

    console.log(user_id);

    MuffinsForUsers.find({
      'userId': user_id
    }, function(err, muffinsArray) {

      var muffinIds = _.pluck(muffinsArray, "muffinId");

      Muffins.find({
        '_id': {
          '$in': muffinIds
        },
        "state": "active"
      }, function(err, muffinDetails) {

        return httpResponse.successResponse(res, {
          muffins: muffinDetails
        });

      });
    });

  }

  exports.getMyMuffinsMobile = function(req, res) {
    var user_id = req.params.user_id;

    getMainMuffins(user_id, res);

  }


  function getMainMuffins(user_id, res) {
    if (!user_id) return Error.errorMissingParam(res, 'user_id');
    var finalObject = {},
    dataArray = [];

    finalObject['type'] = 'bidding_muffins';
    finalObject['count'] = 0;
    finalObject['muffin_details'] = [];
    dataArray.push(finalObject);
    finalObject = {};
    finalObject['type'] = 'invited_muffins';
    finalObject['count'] = 0;
    finalObject['muffin_details'] = [];
    dataArray.push(finalObject);
    finalObject = {};
    finalObject['type'] = 'active_muffins';
    finalObject['count'] = 0;
    finalObject['muffin_details'] = [];
    dataArray.push(finalObject);
    finalObject = {};
    finalObject['type'] = 'in_progress_muffins';
    finalObject['count'] = 0;
    finalObject['muffin_details'] = [];
    dataArray.push(finalObject);

    MuffinsForUsers.find({
      'userId': user_id
    }, function(err, muffinsArray) {
      if (!muffinsArray || muffinsArray.length < 1) return httpResponse.successResponse(res, dataArray);
      var muffinIds = _.pluck(muffinsArray, "muffinId");
      var IndexedConfigs = _.indexBy(muffinsArray, 'muffinId');
      Muffins.find({
        '_id': {
          '$in': muffinIds
        }
      }, function(err, muffinDetails) {
        var activeMuffin = [],
        inProcessMuffin = [],
        liveBiddingMuffin = [],
        invitedMuffins = [];


        return async.eachSeries(muffinDetails, function(muffin, cb) {
          if (IndexedConfigs[muffin['id']].relationType == 'pending') {
            var ObjectToSend = {};
            var id = muffin['id'];
            var createdByUser;
            ObjectToSend['muffin_detail'] = muffin;
            return MuffinsForUsers.find({
              'muffinId': id
            }, function(err, UsersDetails) {
              var userIds = _.pluck(UsersDetails, 'userId');
              var userIdIndexed = _.indexBy(UsersDetails, 'userId');
              Users.find({
                '_id': userIds
              }, function(err, UsersDetailsArray) {
                ObjectToSend['user_details'] = UsersDetailsArray;
                createdByUser = _.find(UsersDetailsArray, function(u) {
                  return u._id.equals(muffin.createdBy);
                });
                if (createdByUser) {
                  var newMuffin = {};
                  newMuffin = JSON.parse(JSON.stringify(muffin));
                  newMuffin.createdByObject = createdByUser;
                  newMuffin.state = 'pending';
                  ObjectToSend['muffin_detail'] = newMuffin;
                  invitedMuffins.push(ObjectToSend);
                  return cb();
                } else {
                  ObjectToSend['muffin_detail'] = muffin;
                  invitedMuffins.push(ObjectToSend);
                  return cb();
                }
              });
            });
  } else if (IndexedConfigs[muffin['id']].relationType == 'subscribed' && muffin['state'] == 'active') {
    var ObjectToSend = {},
    query = {};
    var id = muffin['id'];
    ObjectToSend['muffin_detail'] = muffin;
    return MuffinsForUsers.find({
      'muffinId': id
    }, function(err, UsersDetails) {
      var userIds = _.pluck(UsersDetails, 'userId');
      var userIdIndexed = _.indexBy(UsersDetails, 'userId');
      Users.find({
        '_id': userIds
      }, function(err, UsersDetailsArray) {
        if (muffin.currentBidding == null) {
          muffin.currentBidding = 0;
        }
        if (muffin.currentBidding > 0) {

          query = {
            $or: [{
              'muffinId': muffin._id,
              auctionNumber: muffin.currentBidding
            }, {
              'muffinId': muffin._id,
              auctionNumber: muffin.currentBidding - 1
            }]
          };

        } else {
          query = {
            'muffinId': muffin._id,
            auctionNumber: muffin.currentBidding
          };
        }
        Auctions.find(query, function(err, auctionsData) {
          ObjectToSend['auctions'] = auctionsData;
          ObjectToSend['user_details'] = UsersDetailsArray;
          ObjectToSend['configs'] = UsersDetails;
          createdByUser = _.find(UsersDetailsArray, function(u) {
            return u._id.equals(muffin.createdBy);
          });
          if (createdByUser) {
            var newMuffin = {};
            newMuffin = JSON.parse(JSON.stringify(muffin));
            newMuffin.createdByObject = createdByUser;
            ObjectToSend['muffin_detail'] = newMuffin;
            activeMuffin.push(ObjectToSend);
            return cb();
          } else {
            ObjectToSend['muffin_detail'] = muffin;
            activeMuffin.push(ObjectToSend);
            return cb();
          }
        }).populate("highestBid.user_id").exec();
      });
  })
  } else if (IndexedConfigs[muffin['id']].relationType == 'subscribed' && (muffin['state'] == 'approval_queue' || muffin['state'] == 'subscription_queue'|| muffin['state'] == 'reg_queue_previous_sanction' || muffin['state'] == 'reg_queue_registration' || muffin['state'] == 'reg_queue_commencement' || muffin['state'] == 'reg_queue_completion')) {
    var ObjectToSend = {};
    var id = muffin['id'];
    return MuffinsForUsers.find({
      'muffinId': id
    }, function(err, UsersDetails) {
      var userIds = _.pluck(UsersDetails, 'userId');
      var userIdIndexed = _.indexBy(UsersDetails, 'userId');
      console.log('This is the users', userIds);
      Users.find({
        '_id': {
          '$in': userIds
        }
      }, function(err, UsersDetailsArray) {
        ObjectToSend['user_details'] = UsersDetailsArray;
        createdByUser = _.find(UsersDetailsArray, function(u) {
          return u._id.equals(muffin.createdBy);
        });
        if (createdByUser) {
          var newMuffin = {};
          newMuffin = JSON.parse(JSON.stringify(muffin));
          newMuffin.createdByObject = createdByUser;
          newMuffin.state = 'approval_queue';
          ObjectToSend['muffin_detail'] = newMuffin;
          inProcessMuffin.push(ObjectToSend);
          return cb();
        } else {
          muffin.state = 'approval_queue';
          console.log("  THE MUFFIN NOT GOOD     ");
          console.log(muffin);
          ObjectToSend['muffin_detail'] = muffin;
          inProcessMuffin.push(ObjectToSend);
          return cb();
        }
      });
    });
  } else if (IndexedConfigs[muffin['id']].relationType == 'subscribed' && muffin['state'] == 'bidding_active') {
    var ObjectToSend = {};
    var id = muffin['id'];
    var currentAuctionNumber = muffin.currentBidding;
    return Auctions.findOne({
      'muffinId': id,
      'auctionNumber': currentAuctionNumber
    }, function(err, bidData) {
      ObjectToSend['muffin_detail'] = muffin;
      return MuffinsForUsers.find({
        'muffinId': id
      }, function(err, UsersDetails) {
        var userIds = _.pluck(UsersDetails, 'userId');
        var userIdIndexed = _.indexBy(UsersDetails, 'userId');
        console.log('This is the users', userIds);
        Users.find({
          '_id': {
            '$in': userIds
          }
        }, function(err, UsersDetailsArray) {
          ObjectToSend['user_details'] = UsersDetailsArray;
          ObjectToSend['auction'] = bidData;
          createdByUser = _.find(UsersDetailsArray, function(u) {
            return u._id.equals(muffin.createdBy);
          });
          if (createdByUser) {
            var newMuffin = {};
            newMuffin = JSON.parse(JSON.stringify(muffin));
            newMuffin.createdByObject = createdByUser;
            ObjectToSend['muffin_detail'] = newMuffin;
            liveBiddingMuffin.push(ObjectToSend);
            return cb();
          } else {
            ObjectToSend['muffin_detail'] = muffin;
            liveBiddingMuffin.push(ObjectToSend);
            return cb();
          }
        }).populate("highestBid.user_id bids.user_id", "name pic").exec();
      });
  }).populate("highestBid.user_id bids.user_id", "name pic").exec();

  } else {
    console.log(dataArray);
    return cb();
  }

  }, function(err, finalResult) {



    var finalObject = {};
    dataArray = [];
    finalObject['type'] = 'bidding_muffins';
    finalObject['count'] = liveBiddingMuffin.length;
    finalObject['muffin_details'] = liveBiddingMuffin;
    dataArray.push(finalObject);
    finalObject = {};
    finalObject['type'] = 'invited_muffins';
    finalObject['count'] = invitedMuffins.length;
    finalObject['muffin_details'] = invitedMuffins;
    dataArray.push(finalObject);
    finalObject = {};
    finalObject['type'] = 'active_muffins';
    finalObject['count'] = activeMuffin.length;
    finalObject['muffin_details'] = activeMuffin;
    dataArray.push(finalObject);
    finalObject = {};
    finalObject['type'] = 'in_progress_muffins';
    finalObject['count'] = inProcessMuffin.length;
    finalObject['muffin_details'] = inProcessMuffin;
    dataArray.push(finalObject);
    console.log(dataArray);
    return httpResponse.successResponse(res, dataArray);

  });
  })
  })
  }


  exports.getMuffinMembers = function(req, res) {

    var muffinId = req.params.muffinId;
    var muffinMembers = [];
    MuffinsForUsers.find({
      'muffinId': muffinId
    }, function(err, UsersDetails) {

      var indexedUsers = _.indexBy(UsersDetails, 'userId');
      var userIds = _.pluck(UsersDetails, 'userId');
      console.log('--->', indexedUsers)
      Users.find({
        '_id': userIds
      }, function(err, UsersDetailsArray) {

        if (UsersDetailsArray) {
          for (var i = 0; i < UsersDetailsArray.length; i++) {
            var objectToPush = UsersDetailsArray[i].toObject();
            objectToPush.property = indexedUsers[objectToPush._id]
            muffinMembers.push(objectToPush);
          }
        }
        var defaulterUsers = _.remove(muffinMembers, function(muffinMember) {
          return muffinMember.relationType == 'pending'
        });
        var finaObjectArray = {};
        finaObjectArray.defaulters = defaulterUsers

        finaObjectArray.others = muffinMembers;

        return httpResponse.successResponse(res, finaObjectArray);
      })
    })

  }


  exports.getMuffinMembersMobile = function(req, res) {

    var muffinId = req.params.muffinId;
    var muffinMembers = [];
    MuffinsForUsers.find({
      'muffinId': muffinId
    }, function(err, muffins) {

      if (muffins) {

        return httpResponse.successResponse(res, muffins);
      }


    }).populate("userId").exec();

  }

  exports.getAgreements = function(req, res) {

    var userId = req.params.userId;

    var muffinAgreements = [];

    MuffinsForUsers.find({
      'userId': userId
    }, function(error, muffinIds) {
      if (error) {
        console.log('error');
        console.log(error);
        return httpResponse.errorResponse(res, error, 500);
      } else {
        var excludingIDArray = _.filter(muffinIds, function(m) {
          return m && m.relationType && m.relationType !== 'pending' && m.IsMuffinAgreementSigned == true;
        });

        Muffins.find({
          "_id": {
            $in: _.map(excludingIDArray, 'muffinId')
          }
        }, function(error, muffinResult) {
          if (error) {
            console.error('error');
            console.error(error);
            return httpResponse.errorResponse(res, error, 500);
          } else {
            console.log(muffinResult);
            var objectToPush = {};
            for (var i = 0; i < muffinResult.length; i++) {
              objectToPush = {};
              objectToPush.muffinDetails = muffinResult[i].toObject();
              objectToPush.agreementLink = userId + '_' + muffinResult[i]._id + '.pdf';
              excludingIDArray.forEach(function(value) {
                if (value.muffinId.equals(muffinResult[i]._id)) {
                  console.log(value.muffinId);
                  if (value.muffinAgreementSignedDate) {
                    objectToPush.signedDate = value.muffinAgreementSignedDate;
                  }
                }
              });

              console.log(objectToPush);
              muffinAgreements.push(objectToPush);
            }
            return httpResponse.successResponse(res, {
              muffinAgreements: muffinAgreements
            });
          }
        });
  }
  });
  }


  exports.getAvailableMuffins = function(req, res) {

    var userId = req.params.userId;
    var mainMuffinIds = [];

    var excludedMuffinIds = [];

    var muffins = [];

    MuffinsForUsers.find({
      'userId': userId
    }, function(error, muffinIds) {
      if (error) {
        console.log('error');
        console.log(error);
        return httpResponse.errorResponse(res, error, 500);
      } else {
        var excludingIDArray = _.filter(muffinIds, function(m) {
          return m && m.relationType && m.relationType !== 'pending';
        });
        var indexedMuffins = _.indexBy(_.filter(muffinIds, function(m) {
          return m && m.relationType && m.relationType === 'pending';
        }), 'muffinId');
        Muffins.find({
          "state": "approval_queue",
          "_id": {
            "$nin": _.map(excludingIDArray, 'muffinId')
          }
        }, function(error, muffinResult) {
          if (error) {
            console.error('error');
            console.error(error);
            return httpResponse.errorResponse(res, error, 500);
          } else {
            var userIDArray = _.map(_.filter(muffinResult, function(m) {
              return m && m.createdBy;
            }), 'createdBy');
            Users.find({
              '_id': {
                $in: userIDArray
              }
            }, function(error, userResult) {
              if (error) {
                console.error('error');
                console.error(error);
                return httpResponse.errorResponse(res, error, 500);
              } else {
                for (var i = 0; i < muffinResult.length; i++) {
                  var objectToPush = muffinResult[i].toObject();
                  objectToPush.createdByObject = _.find(userResult, function(u) {
                    return u && u._id.equals(muffinResult[i].createdBy);
                  });
                  objectToPush.property = indexedMuffins[objectToPush._id]
                  muffins.push(objectToPush);
                }
                return httpResponse.successResponse(res, {
                  muffins: muffins
                });
              }
            });
          }

        });
  }
  });
  }


  exports.setDefaulter = function(req, res) {

    console.log(req.body);
    var muffinId = req.body.muffinId;
    var userId = req.body.userId;

    MuffinsForUsers.update({
      muffinId: muffinId,
      userId: userId
    }, {
      $set: {
        relationType: "defaulter"
      }
    }, function(err, data) {


      return httpResponse.successResponse(res, data);

    });


  }


  exports.addMembersToMuffin = function(req, res) {

    MuffinsForUsers.create(req.body, function(muffindata) {
      return httpResponse.successResponse(res, muffindata);

    });


  }

  exports.removeMembersFromMuffin = function(req, res) {
    var muffinId = req.body.muffin_id;
    var userId = req.body.user_id;
    var relationType = req.body.relationship_type;
    var whereQuery = {};
    whereQuery['userId'] = userId;
    whereQuery['muffinId'] = muffinId;
    relationType = 'unsubcribed';
    if (req.body.relationship_type) relationType = req.body.relationship_type;
    MuffinsForUsers.update(whereQuery, {
      '$set': {
        'relationType': 'unsubcribed',
        'muffinId': muffinId,
        'userdId': userdId
      }
    }, {
      upsert: true
    }, function(muffindata) {
      return httpResponse.successResponse(res, muffindata);

    });


  }



  exports.acceptInvite = function(req, res) {

    console.log(req.body);

    var userId = req.body.userId;
    var muffinId = req.body.muffinId;

    var relationType = req.body.relationType;

    req.body.IsMeetingDone = false;
    req.body.MeetingDate = moment().add(2, 'd').hour('5').minute('30').toString();

    MuffinsForUsers.findOneAndUpdate({
      "userId": userId,
      "muffinId": muffinId
    }, req.body, {
      upsert: true,
      setDefaultsOnInsert: true
    }, function(muffindata) {

      console.log(muffindata);

      return httpResponse.successResponse(res, muffindata);

    });

  }



  exports.addInvite = function(req, res) {

    console.log(req.body);

    var userId = req.body.userId;
    var regId = req.body.muffinId;


    var relationType = req.body.relationType;

    req.body.IsMeetingDone = false;
    req.body.MeetingDate = moment().add(2, 'd').hour('5').minute('30').toString();
    console.log(regId);
    Muffins.find({
      'regNumber': regId
    }, function(err, muffinDetails) {
      console.log(muffinDetails);
      console.log(err);
      if (muffinDetails && muffinDetails.length > 0) {
        for (var i = 0; i < muffinDetails.length; i++) {
          var muffinId = muffinDetails[i]._id;
          req.body.muffinId = muffinId;
          console.log("muffin id issasssasscacssascscsccsa" + muffinId);
          MuffinsForUsers.findOneAndUpdate({
            "userId": userId,
            "muffinId": muffinId
          }, req.body, {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true
          }, function(err, muffindata) {

            if (err) {
              console.log("________MUFFIN IS EMPTY_______ 1 2 3 " + err);
              return handleError(res, err);
            }
            console.log(muffindata);

            if (muffindata == null) {
              console.log("________MUFFIN IS EMPTY_______");
              return handleError(res, err);

            }

            console.log(muffindata);
            return httpResponse.successResponse(res, muffindata);

          });
        }
      } else {
        console.log("Not Found");
        return httpResponse.errorResponse(res, "Registration Number Not Valid", 501, "Registration Number Not Valid");
      }
    });

  }


  exports.getMyMuffinsMobile = function(req, res) {
    var user_id = /*req.user.id*/ 'Server';
    var finalArray = [];
    if (!user_id) return Error.errorMissingParam(res, 'user_id');
  MuffinsForUsers.find({ /*'userId' :user_id*/ }, function(err, MuffinsDetails) {
    if (!MuffinsDetails || MuffinsDetails.length < 1) return httpResponse.successResponse(res, MuffinsDetails);
    var muffinIds = _.pluck(MuffinsDetails, "muffinId");
    var IndexedConfigs = _.indexBy(MuffinsDetails, 'muffinId');
    Muffins.find({
      '_id': {
        $in: muffinIds
      }
    }, function(err, muffinDetails) {
      var indexedMuffins = _.indexBy(muffinDetails, '_id');

      for (var i = 0; i <= MuffinsDetails.length; i++) {
        var muffinId = MuffinsDetails[i].muffinId;
        var objectToSend = {};
        MuffinsForUsers.find({
          'muffinId': muffinId
        }, function(err, muffinUsers) {
          var userIdsForMuffins = _.pluck(muffinUsers, "userId");
          Users.find({
            '_id': {
              $in: userIdsForMuffins
            }
          }, function(err, muffinUserDetails) {
            objectToSend.type = indexedMuffins[muffinId]['state'];
            objectToSend.users_details = muffinUserDetails;
            finalArray.push(objectToSend);
          })
        })
        if (i == MuffinsDetails.length - 1) return httpResponse.successResponse(res, finalArray);
      }
    })
  });

  }


  exports.getMyMuffinsMobileTesting = function(req, res) {

    Muffins.find({}, function(err, muffinDetails) {
      return httpResponse.successResponse(res, muffinDetails);
    });


  }


  exports.getMuffinDetail = function(req, res) {
    var muffinId = req.params.muffin_id;
    var whereQuery = {},
    filterData = {};
    whereQuery['_id'] = muffinId;
    var finalObject = {};
    Muffins.find(whereQuery, function(err, muffinInfo) {
      finalObject.muffinDetails = muffinInfo;
      MuffinsForUsers.find({
        'muffinId': muffinId
      }, {
        'userdId': 1,
        '_id': -1
      }, function(err, muffinUserIds) {
        if (muffinUserIds.length < 0) {
          finalObject.usersDetail = [];
          return httpResponse.successResponse(res, finalObject);
        }
        var userIds = _.pluck(muffinUserIds, 'userId');
        Users.find({
          '_id': {
            $in: userIds
          }
        }, function(err, userDetails) {
          finalObject.usersDetail = userDetails;
          return httpResponse.successResponse(res, finalObject);
        })
      })
    })
  }


  exports.getMuffinDetailApp = function(req, res) {
    var muffinId = req.params.muffin_id;
    var whereQuery = {},
    filterData = {};
    whereQuery['_id'] = muffinId;
    var finalObject = {};
    Muffins.findById(whereQuery, function(err, muffinInfo) {
      console.log(muffinInfo.currentBidding + "  m ");
      var query;
      if (muffinInfo.currentBidding == null) {
        muffinInfo.currentBidding = 0;
      }

      MuffinsForUsers.find({
        'muffinId': muffinId
      }, function(error, muffinsForUsersResult) {
        if (error) {
          console.log('error');
          console.log(error);
          return httpResponse.errorResponse(res, error, 500);
        } else {
          var userIDArray = _.map(_.filter(muffinsForUsersResult, function(m) {
            return m && m.userId;
          }), 'userId');
          var meetingDate = _.find(muffinsForUsersResult, function(m) {
            return m && m.userId && m.MeetingDate && m.userId.equals(req.user._id);
          }).MeetingDate;
          var isMeetingDone = _.find(muffinsForUsersResult, function(m) {
            return m && m.userId && m.MeetingDate && m.userId.equals(req.user._id);
          }).IsMeetingDone;

          var isMuffinAgreementSigned = _.find(muffinsForUsersResult, function(m) {
            return m && m.userId && m.userId.equals(req.user._id);
          }).IsMuffinAgreementSigned;
          var muffinsAgreementSignedDate = _.find(muffinsForUsersResult, function(m) {
            return m && m.userId && m.userId.equals(req.user._id);
          }).muffinAgreementSignedDate;
          Users.find({
            '_id': {
              $in: userIDArray
            }
          }, function(error, userResult) {
            if (error) {
              console.log('error');
              console.log(error);
              return httpResponse.errorResponse(res, error, 500);
            } else {
              var createdByObj;
              var totalCount = userResult.length;
              var verifiedCount = 0;
              userResult.forEach(function(user) {
                if (user.kycDone && user.creditAssessmentDone) {
                  verifiedCount += 1;
                }
                if (user._id.equals(muffinInfo.createdBy)) {
                  createdByObj = user;

                }
              });
              var newMuffinDetails = JSON.parse(JSON.stringify(muffinInfo));
              newMuffinDetails.MeetingDate = meetingDate;
              newMuffinDetails.IsMeetingDone = isMeetingDone;
              newMuffinDetails.IsMuffinAgreementSigned = isMuffinAgreementSigned;
              newMuffinDetails.muffinAgreementSignedDate = muffinsAgreementSignedDate;
              newMuffinDetails.VerifiedMembers = verifiedCount;
              newMuffinDetails.MeetingDate = meetingDate;
              newMuffinDetails.TotalMembers = totalCount;
              newMuffinDetails.createdByObject = createdByObj;
              newMuffinDetails.user = req.user;
              finalObject.muffinDetails = newMuffinDetails;
              finalObject.user = req.user;
              if (muffinInfo.currentBidding > 0) {
                query = {
                  $or: [{
                    'muffinId': muffinId,
                    // 'auctionNumber':0
                    //auctionNumber:muffinInfo.currentBidding
                    
                  }, {
                    'muffinId': muffinId,
                    // 'auctionNumber':0
                    //auctionNumber: muffinInfo.currentBidding - 1

                  }]
                };
              } else {
                query = {
                  'muffinId': muffinId,
                  //auctionNumber: muffinInfo.currentBidding
                  // 'auctionNumber':0
                }
              }
              Auctions.find(query, function(err, data) {
                finalObject.auctions = data;
                console.log("===============auction list------>"+ data);

                return httpResponse.successResponse(res, finalObject);

              }).populate("highestBid.user_id bids.user_id").exec();
            }
          });
  }
  });
  });
  }

  exports.rescheduleMeeting = function(req, res) {
    var requiredFields = ['rescheduledDateTime', 'muffinId', 'userId'];
    var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
    if (!paramCheck.success) {
      return Errors.errorMissingParam(res, paramCheck.data.toString());
    } else {
      var whereQuery = {};
      var updateQuery = {};
      var options = {
        upsert: true,
        setDefaultsOnInsert: true
      };
      whereQuery.muffinId = req.body.muffinId;
      whereQuery.userId = req.body.userId;
      updateQuery.MeetingDate = req.body.rescheduledDateTime;
      updateMuffinsUtil(whereQuery, updateQuery, options, function(err, updateResults) {
        if (err) {
          return handleError(res, err);
        }
        return httpResponse.successResponse(res, updateResults);
      });
    }

  }


  exports.getMuffinFaq = function(req, res) {
    var dummyObject = [{
      'Q': 'Why should I subscribe to a Muffin?',
      'A': "Financial planning is all about having enough money to cover for your life’s needs. We buy savings instruments when we have income to spare, or else, we take loans and advances if bigger expenses happen in quick succession. A muffin allows you to fulfil both savings and borrowing needs in one instrument. Our users most often join a Muffin to create a reserve for contingencies like vehicle purchase, expensive travel, family events, luxury expenses, marriages, home improvement etc. Once you join a Muffin, you can go to the Muffin screen and set the purpose of joining the Muffin."
    }, {
      'Q': 'What is the financial sense in joining a Muffin?',
      'A': "The biggest advantage of Muffin is that you can plan in advance, for any forthcoming cash outflow even if you don’t know the exact timing of the event. For example, a marriage may materialise in 5 months, or 15 or 25 months. Soon as you subscribe to Muffin, you are sure to get the money you planned. Whether you need it in the beginning or at the end will impact whether it acts as a loan or as a saving. Similarly, a user can list out all her/his requirements and then subscribe a Muffin for each requirement. Whenever the user needs the money, s/he may prize the respective Muffin without thinking about how to liquidate their savings or where to get a loan from. A Muffin allows a user to break down all their financial needs into monthly instalments."
    }, {
      'Q': 'How do Muffins compare with other financial instruments?',
      'A': "Muffins are intuitive life tools. Whenever a person needs to save up for their future, they are faced with the looming question of where to invest their money. The answer to this is never easy and everyone has their different advice. No matter where you invest, there’s always a catch. If it’s an FD / RD, you lose money if your money need arrives sooner, you end up losing interest. Besides FD / RD is very basic return. If it's the stock market, your investment may go up or it may erode your capital! Property is very illiquid and one needs at least a few months before one can sell. Also, all these asset classes will only give back your money plus return on your money. If your need is bigger than your savings, you will still have to go and apply for a loan at a bank or draw on your credit card. While credit cards are expensive (40% interest!), loans may take forever to get processed. In short, financial planning is tough!\n Muffin is easy! You subscribe to one Muffin for every need. Whenever your need arrives, participate in an auction and take your prize money. If you need money sooner, you will end up paying more than you get. If your need comes later, you will get more than you paid in instalments. It’s that simple! You don’t need to dabble in so many different savings instruments or apply for loans again. Muffin makes financial planning easier and much more accessible."
    }, {
      'Q': 'How can I start / join a Muffin?',
      'A': "There’s a big red ‘plus’ action button at the bottom of your Muffin screen. Clicking it will reveal three option:\n− Add a Muffin\n− Join a Muffin\n− Join by Code\nAdding a Muffin means you make your own Muffin of known people. Joining a Muffin means joining as an individual. By joining you become part of a group of registered Muffin community members. You may or may not know people in your group when you join individually. Join by Code is used when one of your friends invites you to join their Muffin, entering the code they’ve sent you enables you to view the details of the invited group."
    }, {
      'Q': 'How can I participate in my Muffin’s auction?',
      'A': "An ongoing auction window is visible to the user on the Muffin screen. The auction window opens for a duration of 10 minutes. You can set reminders to inform about an upcoming auction. Please make sure you have a stable internet connection during the auction window. Timing of the auction is written on the agreements as well as in the app."
    }, {
      'Q': 'How long does it take for a group to begin?',
      'A': "Registering a group may take about 20 days. This includes completing all documentation, collecting first instalments, standard response times etc. Completing the required number of users in a group depends on several factors and may take upward of 10 days. From conception to first auction, a group may take 30 days or more."
    }, {
      'Q': 'What happens to the agreements we sign with Muffin?',
      'A': "Registering a group is a long and intensive process because it aims to protect public money. Besides giving a 100% liquid security to the regulator, we must also give regular event based reports and all Muffin agreements signed with our subscribers to the Registrar for the Registrar’s record keeping and auditory purposes. The agreements you sign are given in copy to the regulator."
    }, {
      'Q': 'How can I get the agreements I sign?',
      'A': "An agreement signed between us and the subscriber has no value if it isn’t registered. Once the agreements are registered (which may take a while), we automatically make it available for download in the Agreements option in the app sidebar."
    }, {
      'Q': 'Why should I feel secure with Muffin?',
      'A': "Muffin does not keep money with itself, the instalments that we collect are immediately given to the auction winner. Since muffin is transparent and real time, you can see all transactions happening instantly on your app. We are giving every piece of information that a subscriber is entitled to, and even more! You have access to real time reports about prize amount payments, prize winners, defaulters, instalments, auctions, statements, transactions and access to Muffin agreements."
    }, {
      'Q': 'How can I get my friends and family to join a Muffin with me?',
      'A': "You can create a group of your chosen instalment and duration on our platform. Once you structure the group, you can send a text or a WhatsApp invite to join the group to your friends / family / colleagues / golf group / beer buddies (anyone you’re close to). This text will carry a unique group code which your friends can enter after they download Muffin. On entering the code, your friends will see the invited Muffin on their screens."
    }, {
      'Q': 'What happens if I create a Muffin but there’s still more members needed to complete the group?',
      'A': "How many members you want in the group depends on the duration of the group. Since you are free to choose the structure of the group, only select as many members as you are sure you can gather. It is encouraged that you speak with your friends before you invite them to your Muffin, otherwise they may not be able to decide whether to invest their money in the group or not. If you’re still short of a few people, please give us a call. We will give you last resort options."
    }, {
      'Q': 'Is there is a referral program in place?',
      'A': "Absolutely! If you start a group, you get the full face-value of the Muffin in the first month as well as benefits throughout the duration of the Muffin. It’s not just an interest free loan, you get more money upfront than you need to repay!"
    }, {
      'Q': 'What if 2 people bid a similar amount in the last second of the auction window?',
      'A': "There are two aspects of the auction window to take care of this situation. First, the auction window is precise down to milliseconds. If two people bid the same amount, the person who bids first will be the leading bidder. If the other person wishes to win, s/he must bid lower to remain the leading bidder. Second, if a person bids right at the end of the auction window, the auction window extends by 15 seconds. No matter what, every user accessing the bidding window will have at least 15 seconds to respond to the leading bid, even if the leading bid is made in the last second."
    }, {
      'Q': 'I can see the auction window, but I’m unable to bid, what’s happening?',
      'A': "Since each member can only be prized once, if you have already won a bid, you will not be able to bid again for the remaining duration of the Muffin. If you are unprized and yet you are unable to bid in the auction, it may be because there’s a default on your instalments. A defaulter cannot bid because a defaulter is not allowed to win an auction."
    }, {
      'Q': 'How long will it take to receive the prize amount after the auction?',
      'A': "We need to make collections and work out regulatory documentation after every auction. This process takes a up to 10 days. A prize winner will be credited the prize amount within 10 to 12 days."
    }, {
      'Q': 'How do I pay my instalments?',
      'A': "You do not need to worry about paying instalments. All instalments will be processed by ECS. The national electronic clearing system is an automatic direct debit system. It is absolutely necessary to keep the instalment amount extra in your bank at the time of the auction, the instalment will be deducted immediately after the auction is won. In case of bouncing due to insufficient funds, we will contact you directly and request you to make your payment by cheque, NEFT or an online payment gateway. Payment gateways come with an additional transaction fee. We are working on making our app UPI enabled so that you may make your payments using your bank accounts directly from your phone."
    }, {
      'Q': 'What happens if my instalments are not paid on time?',
      'A': "Delayed instalments are time consuming and expensive. If ECS payment is bounced, we grace our subscribers with 7 days’ time to make the bounced payment. We continuously stay in touch with that subscriber and assist in getting the payment processed within the permitted time frame. If, however, the subscriber fails to comply beyond the grace period too, the subscriber becomes a defaulter in the system. This means that everybody in that Muffin group will be able to see that that subscriber has failed to pay the instalment. Besides that, bouncing charges and late fee charges will be levied on the subscriber’s amount to cover for our banking expenses and follow-up cost. Furthermore, interest will be charged for an entire month at 2% as soon as a subscriber turns defaulter. Interest will keep on getting levied every month thereon if the subscriber fails to make payment towards account balances. Long duration non-payments or not responding to a Muffin representative will ensue legal proceedings and permanent banning from our platform."
    }, {
      'Q': 'How do I get a receipt of my payments to Muffin?',
      'A': "All receipts of all transactions are available to the subscriber to download and view in the transactions tab in the reports area of the app."
    }, {
      'Q': 'Where can I manage my Muffins?',
      'A': "A list of all relevant Muffins is visible to the subscriber in the Muffins area of the app. This list contains active muffins, muffins that are in-progress of starting, muffins the user is invited to and muffins in which bidding is going on. More information / options relating to these Muffins can be accessed by clicking on the respective Muffins."
    }, {
      'Q': 'What kind of reports do I get about my Muffins?',
      'A': "You are provided with all information relating to your Muffins in your app. You have the same information as we do! You can see who won which auction at which prize amount, you can see your month-wise statement and account balance, you can download individual receipts of each instalment and much more. Most of this information is available in the reports section of the app."
    }, {
      'Q': 'Can I become a part subscriber to a Muffin?',
      'A': "Unfortunately, not right now. We may come up with this and other new features in the future."
    }, {
      'Q': 'I want to subscribe to bigger Muffins for my bigger needs. What should I do?',
      'A': "We offer bigger Muffins as part of our secured traditional business, which works as a trade partner with us. For bigger Muffins, we will need our users to provide sufficient security before prizing of their auction. This security may include insurance policies or mortgages."
    }, {
      'Q': 'Can students subscribe to Muffins?',
      'A': "As long as the student has a stable source of income, sure, they can subscribe to a Muffin."
    }, {
      'Q': 'Can housewives join a Muffin, like a kitty group?',
      'A': "Most certainly! Kitties are historically a large part of household financing (and a source of enjoyment, doubling up as social engagement) in India. If the group personally guarantees the instalments of its members, a kitty group gets a lot of benefits from Muffin! P.S. Guaranteeing means that any default will be the responsibility of the group and not Muffin."
    }, {
      'Q': 'When can I renew my Muffin membership?',
      'A': "When to renew a Muffin subscription is purely the choice of the user. Muffin is all about how well you plan and organise yourself. If you see a need coming up in the near future, it is prudent financial planning to save up till your need. Muffin is an excellent instrument for planning financial events."
    }, {
      'Q': 'How do I take care of the Muffin accounting in my books of accounts?',
      'A': "Sit back and relax. Muffins don’t impact your accounting up until they are finished. We will send you the necessary reports that you can give to your CA (or use it yourself for calculating your taxes). We will guide you when the time comes!"
    }, {
      'Q': 'Do you deduct any TDS from my earnings in Muffin?',
      'A': "None whatsoever!"
    }, {
      'Q': 'What should I do if I am changing my residence?',
      'A': "Please inform us immediately through our contact form online or on the app. Or else, please write to us at our email ‘help@muffinapp.in’."
    }, {
      'Q': 'What if I have any other questions?',
      'A': "We encourage you to write to us through our contact form online or on the app. Or else, please feel free to email us at ‘help@muffinapp.in’."
    }]
    var finalObject = {}

    finalObject.results = dummyObject;
    return httpResponse.successResponse(res, finalObject);

  }

  exports.editMyConfigs = function(req, res) {
    var whereQuery = {},
    updateQuery = {},
    options = {};
    var muffinId = req.body.muffin_id;
    whereQuery['muffinId'] = muffinId;
    var userId = req.body.user_id; //@todo have to remove same
    whereQuery['userId'] = userId //@todo have to remove same
    var oneDay = false,
    twoHour = false;
    if (req.body.one_day_before) oneDay = true;
    if (req.body.two_hours_before) twoHour = true;
    var bidding_reminder_configs = {};
    bidding_reminder_configs['one_day_before'] = oneDay;
    bidding_reminder_configs['two_hours_before'] = twoHour;


    updateQuery = {
      '$set': bidding_reminder_configs
    }
    updateMuffinsUtil(whereQuery, updateQuery, options, function(err, updateResults) {
      console.log(err, JSON.stringify(updateResults));
      if (err) {
        return handleError(res, err);
      }
      return httpResponse.successResponse(res, updateResults);
    });
  }

  exports.getMyMuffinConfigs = function(req, res) {

    var muffinId = req.params.muffin_id;
    var whereQuery = {};

    var userId = req.params.user_id; //@todo have to remove same
    whereQuery['muffinId'] = muffinId;
    whereQuery['userId'] = userId //@todo have to remove same

    var dataToSend = {};

    MuffinsForUsers.findOne(whereQuery, function(err, muffinConfigs) {

      if (err) {

        return handleError(res, err);
      }


      Muffins.findById(muffinId, function(err, data) {

        if (data) {

          var currentAuctionNumber = data.currentAuctionNumber;

          if (currentAuctionNumber == null) {
            currentAuctionNumber = 0;
          }

          Auctions.find({
            'muffinId': muffinId,
            'auctionNumber': currentAuctionNumber
          }, function(err, data) {

            console.log("----------------------------------------------"+data);

            if (data) {
              var auctionDate = data.auctionDate;

              muffinConfigs.auctionDate = auctionDate;

              dataToSend = {
                muffinConfigs: muffinConfigs,
                auctionDate: auctionDate

              }

              console.log(err, dataToSend);
            } else {
              dataToSend = {
                muffinConfigs: muffinConfigs

              }
            }


            return httpResponse.successResponse(res, dataToSend);

          });

        }

      })


    })
  }


  exports.transferMyMuffin = function(req, res) {
    var muffinId = req.params.muffin_id;
    var whereQuery = {};
    var userId = req.body.user_id;
    var newUserId = req.body.new_user_id;
    var whereQuery = {},
    updateQuery = {},
    options = {};
    whereQuery['userId'] = userId;
    whereQuery['muffinId'] = muffinId;
    updateQuery = {
      '$set': {
        'is_transferred': true,
        'transferred_to': newUserId
      }
    };
    updateMuffinsUtil(whereQuery, updateQuery, options, function(err, updateResults) {
      if (err) {
        return handleError(res, err);
      }
      return httpResponse.successResponse(res, updateResults);
    });

  }

  function updateMuffinsUtil(whereQuery, data, options, callback) {

    console.log(whereQuery, data, options);

    MuffinsForUsers.findOneAndUpdate(whereQuery, data, options, function(err, object) {
      console.log(err);

      if (err) callback(err, false)
        if (object == null) return callback('No Data found', false);
      callback(null, object);
    });



  }


  function checkForMuffinAgree(whereQuery, data, options, callback) {

    console.log(whereQuery);

    MuffinsForUsers.findOneAndUpdate(whereQuery, data, options, function(err, object) {
      console.log(err);

      if (err) callback(err, false)
        if (object == null) return callback('No Data found', false);
      callback(null, object);
    });



  }

  exports.checkForMuffinAgreement = function(req, res) {
    var whereQuery = {};
    var muffinId = req.body.muffin_id;
    var userId = req.body.user_id;

    whereQuery['userId'] = userId;
    whereQuery['muffinId'] = muffinId;
    console.log(whereQuery);
    return false;
    MuffinsForUsers.find(whereQuery, {
      IsMuffinAgreementSigned: "true"
    }, function(err, signed) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      res.status(200).send({
        status: "success",
        response: signed
      });

    });
  }

  exports.muffinState = function(req, res) {
    var requiredFields = ['muffinId', 'name'];
    var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
    if (!paramCheck.success) {
      return Errors.errorMissingParam(res, paramCheck.data.toString());
    } else {
      MuffinsForUsers.findOne({
        _id: req.body.muffinId
      }, function(error, muffinResult) {

        if (error) {
          handleError(res, error);
        } else {
          if (!muffinResult || muffinResult === {}) {
            Success.errorResponse(res, 'Something went wrong', 500, 'Invalid Muffin');

          } else {
            var createObj = {};
            createObj.name = req.body.name;
            createObj.userId = req.body.userId;
            createObj.muffinId = req.body.muffinId;
            createObj.craetedByAdminType = req.body.craetedByAdminType;
            createObj.stateAcceptDecline = req.body.stateAcceptDecline;
            createObj.state = req.body.state;
            var state = createObj.state;

            if (state == 'reg_queue_previous_sanction' || 'approval_queue') {
              if (createObj.craetedByAdminType == 'admin' || 'superadmin') {
                createObj.state = 'reg_queue_previous_sanction';
              }
              if (state == 'reg_queue_previous_sanction') {
                if (createObj.stateAcceptDecline == true) {
                  createObj.state = 'reg_queue_registraion';
                }
              }
              if (state == 'reg_queue_registraion') {
                if (createObj.stateAcceptDecline == true) {
                  createObj.state = 'reg_queue_commencement';
                }
              }
              if (state == 'reg_queue_commencement') {
                if (createObj.stateAcceptDecline == true) {
                  createObj.state = 'reg_queue_completion';
                }
              }
              if (state == 'reg_queue_completion') {
                if (createObj.stateAcceptDecline == true) {
                  createObj.state = 'active';
                }
              }
              if (state == 'active') {
                if (createObj.stateAcceptDecline == true) {
                  createObj.state = 'bidding_active';
                }
              }
              if (state == '') {
                if (createObj.stateAcceptDecline == false) {
                  createObj.state = 'rejected_at_approval';
                }
              }

              MuffinsForUsers.findByIdAndUpdate(createObj.muffinId, createObj, {
                new: true
              }, function(error, createResult) {

                if (error) {
                  res.status(200).send({
                    status: "error",
                    message: "Something went wrong, Please try again.",
                    errorInfo: err
                  })
                } else {
                  return res.status(200).send({
                    status: "success",
                    response: createResult
                  });
                }

              });
            }
          }
        }
      });

  }
  };


  exports.getAvailableMuffinsState = function(req, res) {

    var userId = req.params.userId;
    var mainMuffinIds = [];

    var excludedMuffinIds = [];

    var muffins = [];

    MuffinsForUsers.find({
      'userId': userId
    }, function(error, muffinIds) {
      if (error) {
        console.log('error');
        console.log(error);
        return httpResponse.errorResponse(res, error, 500);
      } else {
        var excludingIDArray = _.filter(muffinIds, function(m) {
          return m && m.relationType && m.relationType !== 'pending';
        });
        var indexedMuffins = _.indexBy(_.filter(muffinIds, function(m) {
          return m && m.relationType && m.relationType === 'pending';
        }), 'muffinId');
        Muffins.find({

          "_id": {
            "$nin": _.map(excludingIDArray, 'muffinId')
          }
        }, function(error, muffinResult) {
          if (error) {
            console.error('error');
            console.error(error);
            return httpResponse.errorResponse(res, error, 500);
          } else {
            var userIDArray = _.map(_.filter(muffinResult, function(m) {
              return m && m.createdBy;
            }), 'createdBy');
            Users.find({
              '_id': {
                $in: userIDArray
              }
            }, function(error, userResult) {
              if (error) {
                console.error('error');
                console.error(error);
                return httpResponse.errorResponse(res, error, 500);
              } else {
                for (var i = 0; i < muffinResult.length; i++) {
                  var objectToPush = muffinResult[i].toObject();
                  objectToPush.createdByObject = _.find(userResult, function(u) {
                    return u && u._id.equals(muffinResult[i].createdBy);
                  });
                  objectToPush.property = indexedMuffins[objectToPush._id]
                  muffins.push(objectToPush);
                }
                return httpResponse.successResponse(res, {
                  muffins: muffins
                });
              }
            });
          }

        });
  }
  });
  }