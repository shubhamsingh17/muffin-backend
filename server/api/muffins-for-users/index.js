'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./muffins-for-users.controller');
var auth = require('../../auth/auth.service');


router.get('/', controller.index);
router.get('/:id', controller.show);

router.post('/', controller.create);
router.post('/me/assign/muffin', controller.assignMufffin);

router.put('/:id', controller.update);

router.delete('/:id', controller.destroy);

router.get('/get/users/muffins', auth.isAuthenticated(), controller.getMyMuffins);
router.get('/get/users/active/muffins', auth.isAuthenticated(), controller.getActiveMuffins);
router.get('/get/users/muffins/:user_id', controller.getMyMuffinsMobile);
router.get('/getAvailableMuffins/:userId', controller.getAvailableMuffins);
router.get('/allMuffinStates/:userId', controller.getAvailableMuffinsState);
router.get('/getAgreements/:userId', controller.getAgreements);

router.get('/getMuffinMembers/:muffinId', controller.getMuffinMembers);
router.get('/getMuffinMembersMobile/:muffinId', controller.getMuffinMembersMobile);
router.post('/setDefaulter', controller.setDefaulter);
router.post('/addMembersToMuffin', controller.addMembersToMuffin);
router.put('/remove/muffin/member', controller.removeMembersFromMuffin);
router.put('/reschedule/muffin/meeting', controller.rescheduleMeeting);


router.put('/accept/invite', controller.acceptInvite);
router.put('/add/invite', controller.addInvite);

router.get('/get/users/mobile/muffins', controller.getMyMuffinsMobile);
router.get('/get/muffin/details/:muffin_id', controller.getMuffinDetail);
router.get('/getMuffinDetailApp/:muffin_id', auth.isAuthenticated(), controller.getMuffinDetailApp);
router.get('/get/muffin/faq', controller.getMuffinFaq);
router.post('/testPush', controller.testPush);
router.put('/set/muffin/configs', controller.editMyConfigs);
router.get('/my/muffin/configs/main/config/:muffin_id/:user_id', controller.getMyMuffinConfigs);
router.put('/tranfer/my/muffin/:muffin_id', controller.transferMyMuffin);
router.post('/checkMuffinAgreement', controller.checkForMuffinAgreement);
router.post('/state', controller.muffinState);
module.exports = router;