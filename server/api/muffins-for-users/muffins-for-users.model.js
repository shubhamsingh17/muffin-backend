'use strict';

var mongoose = require('mongoose');
var ticketNumber = require('./muffins-for-ticket.model');


var Schema = mongoose.Schema;

var MuffinsForUsersSchema = new Schema({

  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  muffinId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Muffin'
  },
  bidding_reminder_configs: {
    one_day_before: {
      type: Boolean,
      default: false
    },
    two_hours_before: {
      type: Boolean,
      default: true
    }
  },

  relationType: {
    type: String,
    default: 'pending'
  }, // subscribed, defaulter, declined, pending, invited

  defaulter: {

    amount: Number,
    auctionId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Auctions'
    }

  },
  is_transferred: {
    type: Boolean,
    default: false
  },
  transferred_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  MeetingDate: {
    type: String,
    default: ''
  }, //TODO: refactor this to date type
  IsMeetingDone: {
    type: Boolean,
    default: false
  },
  IsMuffinAgreementSigned: {
    type: Boolean,
    default: false
  },
  muffinAgreementSignedDate: {
    type: Date
  },
  ticketNo: {
    type: Number
  }


});

MuffinsForUsersSchema.pre('save', function(next) {
  var doc = this;

ticketNumber.findByIdAndUpdate({_id: 'caId'},{$inc: { seq: 1}},{"upsert": true,"new": true  }, function(error, counter)   {
    if(error)
        return next(error);
    doc.ticketNo = counter.seq;
    next();
});

});

module.exports = mongoose.model('MuffinsForUsers', MuffinsForUsersSchema);