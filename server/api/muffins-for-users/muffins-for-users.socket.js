'use strict';

var MuffinsForUsers = require('./muffins-for-users.model');

exports.register = function (socket) {

  MuffinsForUsers.schema.post('save', function (doc) {
    socket.emit('MuffinsForUsers:save', doc);
  });

  MuffinsForUsers.schema.post('remove', function (doc) {
    socket.emit('MuffinsForUsers:remove', doc);
  });

};
