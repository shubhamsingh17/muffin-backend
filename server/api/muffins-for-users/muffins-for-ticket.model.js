var mongoose = require('mongoose');


var ticketNumberSchema = mongoose.Schema({
   _id: {type: String, required: true},
   seq: {type: Number, default: 1}

});

module.exports = mongoose.model('ticketNumber', ticketNumberSchema);