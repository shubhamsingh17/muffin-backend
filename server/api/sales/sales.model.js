'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SalesSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		lowercase: true,
		required: true
	},
	phone: {
		type: String,
		required: true
	},
	address: {
		type: String
	},
	volumeExpected: {
		type: Number,
		required: true
	},
	valuePerPerson: {
		type: Number
	},
	nextFollowing: {
		type: Date,
		default: +new Date()
	},
	executive: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'MuffinOfficeUsers'
		
	},
	details: {
		type: String
	}
});

module.exports = mongoose.model('Sales', SalesSchema);