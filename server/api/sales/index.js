'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./sales.controller');
var auth = require('../../auth/auth.service');

router.get('/', controller.index); // allow only superadmin to access this route
router.post('/create', controller.create); // allow only usertype admin to create
router.get('/:id', controller.show);
router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);


module.exports = router;