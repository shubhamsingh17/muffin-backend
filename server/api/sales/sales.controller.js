'use strict';

var _ = require('lodash');
var Sales = require('./sales.model');
var Error = require('../../error');
var httpResponse = require('../../responses');

var MuffinOfficeUsers = require('../muffin-office-users/muffin-office-users.model');

function handleError(res, err) {

	return res.status(500).send(err);
}

/**
 * Get list of Sales //superadmin from muffin-office-users can have access or 
 *
 * @param req
 * @param res
 */
exports.index = function(req, res) {
	Sales.find(function(err, sales) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		return res.status(200).send({
			status: "success",
			response: sales
		});
	});
};

/**
 * Get a single Sales // admin from muffin-office-users have access
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
	Sales.findById(req.params.id, function(err, sales) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		if (!sales) {
			res.status(404).send({
				status: "error",
				message: "we are unable to find your request.",
				errorinfo: err
			});
		}
		return res.status(200).send({
			status: "success",
			response: sales
		});
	});
};

/**
 * Creates a new Sales in the DB. // only admin can create
 * 
 * @param req
 * @param res
 */

exports.create = function(req, res) {
	Sales.create(req.body, function(err, sales) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
		}
		return res.status(200).send({
			status: "success",
			response: sales
		});
	});
};

/**
 * Updates an existing Sales in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
	if (req.body._id) {
		delete req.body._id;
	}
	Sales.findById(req.params.id, function(err, sales) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		if (!sales) {
			res.status(404).send({
				status: "error",
				message: "we are unable to find your request.",
				errorinfo: err
			});
		}
		var updated = _.merge(sales, req.body);
		updated.save(function(err) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Something went wrong here.",
					errorInfo: err
				});
			}
			return res.status(200).send({
				status: "success",
				response: sales
			});
		});
	});
};

/**
 * Deletes a Sales from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
	Sales.findById(req.params.id, function(err, slaes) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong here.",
				errorInfo: err
			});
		}
		if (!sales) {
			res.status(404).send({
				status: "error",
				message: "we are unable to find your request.",
				errorinfo: err
			});
		}
		sales.remove(function(err) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Something went wrong here.",
					errorInfo: err
				});
			}
			return res.status(200).send({
				status: "success",
				message: "deleted successfully"
			});
		});
	});
};

/**
 * Search Sales from the DB.
 *
 * @param req
 * @param res
 */