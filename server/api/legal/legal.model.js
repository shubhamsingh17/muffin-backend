'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var caseNum = require('./legal.case.model');
var LegalSchema = new Schema({
  caseNumber: {
    type: Number

  },
  courtOf: {
    type: String
  },
  lastDate: {
    type: Date,
    default: +new Date()
  },
  nextDate: {
    type: Date,
    default: +new Date() + 24 * 60 * 60 * 1000
  },
  sattelmentAmount: {
    type: Number
  },
  location: {
    type: String
  },
  details: {
    type: String
  },
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  userName: {
    type: String
  },
  publish: {
    type: Boolean
  }
});
LegalSchema.pre('save', function(next) {
  var doc = this;

  caseNum.findByIdAndUpdate({
    _id: 'caId'
  }, {
    $inc: {
      seq: 1
    }
  }, {
    "upsert": true,
    "new": true
  }, function(error, counter) {
    if (error)
      return next(error);
    doc.caseNumber = counter.seq;
    next();
  });

});

module.exports = mongoose.model('Legals', LegalSchema);