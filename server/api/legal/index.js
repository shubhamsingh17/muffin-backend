'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./legal.controller');
var auth = require('../../auth/auth.service');

router.get('/',  controller.index); // find all legals 
router.get('/:id', controller.show); // find legal by id
router.post('/getLegalPerMuffin', controller.getLegalPerMuffin); // get legal per muffin and user
router.post('/newLegal', controller.create); //create legal
router.put('/:id', controller.update); //update legal
router.delete('/:id', controller.destroy); //delete legal
router.post('/allSearchLegals',controller.allSearchLegals) // search all legals by text
//previous and next date data store - api yet to create

module.exports = router;