'use strict';

var _ = require('lodash');
var Muffins = require('../muffin/muffin.model');
var Legal = require('./legal.model');
var httpResponse = require('../../responses');
var User = require('../user/user.model');
var Success = require('../../responses');
var Error = require('../../error');

function handleError(res, err) {
	return res.status(500).send(err);
}

/**
 * Get list of Legals
 *
 * @param req
 * @param res
 */
exports.index = function(req, res) {
	Legal.find(function(err, legal) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
		}
		return res.status(200).send({
			status: "success",
			response: legal
		});
	});
};

/**
 * Get a single Legal
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
	Legal.findById(req.params.id, function(err, legal) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
		}
		if (!legal) {
			res.status(404).send({
				status: "error",
				message: "no legal available here.",
				errorinfo: err
			});
		}
		return res.status(200).send({
			status: "success",
			response: legal
		});
	});
};

/**
 * Creates a new Legal in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
	Legal.create(req.body, function(err, legal) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
		}
		return res.status(200).send({
			status: "success",
			response: legal
		});
	});
};

/**
 * Updates an existing Legal in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
	if (req.body._id) {
		delete req.body._id;
	}
	Legal.findById(req.params.id, function(err, legal) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
		}
		if (!legal) {
			res.status(404).send({
				status: "error",
				message: "no legal available here.",
				errorinfo: err
			});
		}
		var updated = _.merge(legal, req.body);
		updated.save(function(err) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Something went wrong, Please try again.",
					errorInfo: err
				})
			}
			return res.status(200).send({
				status: "success",
				response: legal
			});
		});
	});
};

/**
 * Deletes a Legal from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
	Legal.findById(req.params.id, function(err, legal) {
		if (err) {
			res.status(200).send({
				status: "error",
				message: "Something went wrong, Please try again.",
				errorInfo: err
			})
		}
		if (!legal) {
			res.status(404).send({
				status: "error",
				message: "no legal available here.",
				errorinfo: err
			});
		}
		legal.remove(function(err) {
			if (err) {
				res.status(200).send({
					status: "error",
					message: "Something went wrong, Please try again.",
					errorInfo: err
				})
			}
			return res.status(200).send({
				status: "success",
				message: "deleted successfully"
			});
		});
	});
};

/**
 * Get all Legals per muffinId.
 *
 * @param req
 * @param res
 */
exports.getLegalPerMuffin = function(req, res) {

	var muffinId = req.params.muffinId;
	var userId = req.params.userId;
	Legal.find({
		"muffin_id": muffinId,
		"user_id": userId
	}, function(err, legal) {

		return httpResponse.successResponse(res, legal);
	});
};

/**
 * Search for all Legal.
 *
 * @param req
 * @param res
 */
function escapeRegex(text) {
	return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};

exports.allSearchLegals = function(req, res) {
	try {
		if (req.body.query && req.body.query != "") {

			var limit = 12;

			var search = new RegExp(escapeRegex(req.body.query), 'gi');

			var orQueries = [{
					courtOf: new RegExp(search, 'i')
				},

				{
					details: new RegExp(search, 'i')
				}, {
					location: new RegExp(search, 'i')
				}
			];

			Legal.find({

					$or: orQueries

				})
				.limit(limit)

			.exec(function(err, legals) {
				if (err) {
					res.status(200).send({
						status: "error",
						message: "Something went wrong while searching Legals. Please try again.",
						errorInfo: err
					});
				} else {
					res.status(200).send({
						status: "success",
						response: legals
					});
				}

			});
		} else {
			res.status(200).send({
				status: "error",
				message: "Search term required."
			});
		}
	} catch (err) {
		res.status(500).send({
			status: "error",
			message: "Something went wrong while searching legals. Please try again."
		});
	}
}

/**
 * Validation for Muffin_id.
 *
 * @param req
 * @param res
 */