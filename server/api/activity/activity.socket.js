'use strict';

var Activity = require('./activity.model');

exports.register = function (socket) {

  Activity.schema.post('save', function (doc) {
    socket.emit('Activity:save', doc);
  });

  Activity.schema.post('remove', function (doc) {
    socket.emit('Activity:remove', doc);
  });

};
