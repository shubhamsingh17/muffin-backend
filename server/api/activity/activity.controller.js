'use strict';

var _ = require('lodash');
var Activity = require('./activity.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Email = require('../../utils/emailSender');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of Activity
 *
 * @param req
 * @param res
 */
exports.index = function (req, res) {
  Activity.find(function (err, activitys) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(activitys);
  });
};

/**
 * Get a single Activity
 *
 * @param req
 * @param res
 */
exports.show = function (req, res) {
  Activity.findById(req.params.id, function (err, activity) {
    if (err) {
      return handleError(res, err);
    }
    if (!activity) {
      return res.status(404).end();
    }
    return res.status(200).json(activity);
  });
};

/**
 * Creates a new Activity in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function (req, res) {
  Activity.create(req.body, function (err, activity) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(activity);
  });
};

/**
 * Updates an existing Activity in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Activity.findById(req.params.id, function (err, activity) {
    if (err) {
      return handleError(res, err);
    }
    if (!activity) {
      return res.status(404).end();
    }
    var updated = _.merge(activity, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(activity);
    });
  });
};

/**
 * Deletes a Activity from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function (req, res) {
  Activity.findById(req.params.id, function (err, activity) {
    if (err) {
      return handleError(res, err);
    }
    if (!activity) {
      return res.status(404).end();
    }
    activity.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).end();
    });
  });
};

exports.getMyActivities = function (req, res) {
  var whereQuery = {};
  var userId = req.params.user_id;
  whereQuery['user_id'] = userId;

  var query = {
    $or: [{'user_id': userId}, {
      'isGlobal': true
    }]
  }
  // whereQuery['state'] = 'ACTIVE' // @todo will have to uncomment the same
  Activity.find(query, function (err, getMyActivities) {
    return httpResponse.successResponse(res, getMyActivities);
  })
}


exports.createUserActivity = function (req, res) {

  if (!req.body.isGlobal) {
    if (!req.body.user_id)  return Error.errorMissingParam(res, 'user_id');
    if (!req.body.muffin_id)  return Error.errorMissingParam(res, 'muffin_id');
  }
  
  if (!req.body.activity_type)  return Error.errorMissingParam(res, 'activity_type');

  Activity.create(req.body, function (activityData) {
    return httpResponse.successResponse(res, activityData);
  });
}


exports.editMyActivity = function (req, res) {

  console.log(req.body);

  var activityId = req.params.activity_id;
  var whereQuery = {}, updateQuery = {}, options = {};
  whereQuery['_id'] = activityId;
  Activity.findById(activityId, function (err, userDocument) {
    if (!userDocument) handleError(res, 'No User found');
    var updated = _.merge(userDocument, req.body);
    var editedProfile = new Activity(updated, {_id: false});
    editedProfile.save(function (errSave, data) {
      if (errSave) {
        return handleError(res, errSave);
      }

      var message = "user id: " + userDocument.userId + "\n" + "muffin id: " + userDocument.muffinId +
        "\n" + "Muffin name: " + "\n" + "Time: " +
        userDocument.activity_configs.activity_time_slot;
      Email.activity(message);

      return httpResponse.successResponse(res, data);
    })

  })


}
