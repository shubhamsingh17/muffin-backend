'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ActivitySchema = new Schema({
  user_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  muffin_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Muffin'},
  activity_heading: String,
  activity_content: String,
  muffin_name: String,
  isGlobal: {type: Boolean, default: false},
  activity_type: {type: String, enum: ['REFER', 'RESCHEDULE', 'SCHEDULE', 'ALERT', 'VIEW']},
  state: {type: String, enum: ['ACTIVE', 'INACTIVE'], default: 'ACTIVE'},
  created_at: {type: Date, default: Date.now},
  target: {}, // have to figure that,
  activity_configs: {
    activity_time_slot: {type: Date}
  }
});

module.exports = mongoose.model('Activity', ActivitySchema);
