'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./activity.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);

router.post('/', controller.create);

router.put('/:id', controller.update);

router.delete('/:id', controller.destroy);

router.get('/get/my/activities/:user_id', controller.getMyActivities);
router.put('/edit/my/activity/:activity_id', controller.editMyActivity);
router.post('/create/user/activity', controller.createUserActivity);

module.exports = router;
