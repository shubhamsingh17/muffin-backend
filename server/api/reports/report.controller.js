'use strict';

var _ = require('lodash');
var Report = require('./report.model');
var Muffins = require('../muffin/muffin.model');
var Users = require('../user/user.model');
var Auctions = require('../auctions/auctions.model');
var httpResponse = require('../../responses');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var async = require('async');
var utils = require('../../utils/utilityFunctions.js');
var moment = require('moment');


function handleError(res, err) {
  return res.status(500).send(err);
}

/**
 * Get list of  report
 *
 * @param req
 * @param res
 */


exports.index = function(req, res) {
  Report.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Get a single  report
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  Report.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Creates a new  report in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {
  Report.create(req.body, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    return res.status(200).send({
      status: "success",
      response: data
    });
  });
};

/**
 * Updates an existing  report in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Report.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    var updated = _.merge(data, req.body);
    updated.save(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        response: data
      });
    });
  });
};

/**
 * Deletes a  report from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  Report.findById(req.params.id, function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }
    if (!data) {
      return res.status(404).end();
    }
    Report.remove(function(err) {
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      }
      return res.status(200).send({
        status: "success",
        message: "Successfully deleted"

      })
    });
  });
};


exports.chargesOnBounce = function(req, res) {
  if (!req.params._id) {
    // console.log(req);
  } else {
    Report.findById(_id, function(err, charges) {
      console.log(err);
      console.log(charges);
      if (err) {
        res.status(200).send({
          status: "error",
          message: "Something went wrong, Please try again.",
          errorInfo: err
        })
      } else {
        console.log(charges);
      }
      var dataObj = {};

    })
  }
}