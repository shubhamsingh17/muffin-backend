'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./report.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/new', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.get('/charges/:id', controller.chargesOnBounce);
module.exports = router;