'use strict';

var _ = require('lodash');
var moment = require('moment');
var Muffin = require('./muffin.model');
var Error = require('../../error');
var Errors = require('../../error');
var Success = require('../../responses');
var httpResponse = require('../../responses');

var MuffinsForUsers = require('../muffins-for-users/muffins-for-users.model');
var MuffinOfficeUsers = require('../muffin-office-users/muffin-office-users.model');
var Auctions = require('../auctions/auctions.model');
var GetDeeplinkUrl = require('../../utils/getDeeplinkUrl');
var utils = require('../../utils/utilityFunctions.js');

var Client = require('node-rest-client').Client;
var client = new Client();
var schedule = require('node-schedule');
var randtoken = require('rand-token').generator({
  chars: '0-9'
});


function handleError(res, err) {

  return res.status(500).send(err);
}


exports.index = function(req, res) {
  Muffin.find(function(err, sales) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong here.",
        errorInfo: err
      });
    }
    return res.status(200).send({
      status: "success",
      response: sales
    });
  });
};

/**
 * Get list of Muffin
 *
 * @param req
 * @param res
 
 
 */

function findMuffinUtil(whereQuery, selectedOptions, limit, sortBy, skip, cb) { //util for findMuffin

  if (!limit) limit = 10;
  if (!sortBy) sortBy = '-created_at';
  if (!skip) skip = 0;

  Muffin.find(whereQuery, selectedOptions).limit(limit).sort(sortBy)
    .exec(function(err, muffins) {
      cb(null, muffins)
    });

}


function updateMuffinsUtil(whereQuery, data, options, callback) {
  Muffin.findOneAndUpdate(whereQuery, data, options, function(err, object) {

    if (err) callback(err, false);
    if (object == null) return callback('No Data found', false);
    callback(null, object);
  });
}

exports.createMufffin = function(req, res) {

  var userId = req.user._id; // @todo have to uncomment the same
  req.body.createdBy = userId;
  req.body.regNumber = randtoken.generate(5);
  // if (!req.body.regNumber) return Error.errorMissingParam(res, 'regNumber'); //2todo have to add more checks once flow is done

  console.log('------>', req.body);
  Muffin.create(req.body, function(err, muffin) {
    if (err) return httpResponse.errorResponse(res, err, 500);

    console.log('This is --->', muffin);
    var muffinId = muffin['_id'];
    console.log('Current User', req.user);
    var muffinForUserObject = {};
    muffinForUserObject.userId = userId;
    muffinForUserObject.muffinId = muffinId;
    muffinForUserObject.relationType = 'subscribed';
    muffinForUserObject.startsIn = 40; //ßåHardcoded Starts in date
    if (!req.user.kycDone || !req.user.creditAssessmentDone) {
      muffinForUserObject.MeetingDate = moment().add(2, 'd').hour('5').minute('30').toString();
      muffinForUserObject.IsMeetingDone = false;
    } else {
      muffinForUserObject.MeetingDate = moment().add(2, 'd').hour('5').minute('30').toString();
      muffinForUserObject.IsMeetingDone = true;
    }
    console.log('------->', muffinForUserObject);
    MuffinsForUsers.create(muffinForUserObject, function(err, muffincreated) {

      console.log("vvjvajsvdjas11111111111" + muffin);

      GetDeeplinkUrl.getUrl(muffinId, userId, muffin.name, function(url) {
        console.log("vvjvajsvdjas222222222222222" + muffin);

        muffin.link = url;

        muffin.save();

        return httpResponse.successResponse(res, {
          muffin: muffin
        });

      });

    });


  });

};


exports.activateMuffin = function(req, res) {

  // @todo have to get muffin bid dates using this dates
  var whereQuery = {},
    updateQuery = {},
    options = {};
  var muffinId = req.body.muffin_id;
  var auction_date = req.body.auction_date;
  whereQuery['_id'] = muffinId;

  updateQuery = {
    '$set': {
      'state': 'active'
    }
  }


  updateMuffinsUtil(whereQuery, updateQuery, options, function(err, updateResults) {

    if (err) {
      return handleError(res, err);
    }

    createAuctionObjects(muffinId, updateResults.members, auction_date);

    return httpResponse.successResponse(res, updateResults);
  });

};


exports.findMuffins = function(req, res) {

  var whereQuery = {},
    selectedOptions = {},
    limit = 10,
    page = 0,
    sortBy, skip;

  if (req.params.muffin_id) whereQuery['_id'] = req.params.muffin_id;
  if (req.params.muffin_state) whereQuery['state'] = req.params.muffin_state;
  if (req.params.limit) limit = req.params.limit;
  if (req.params.page) page = req.params.page;


  findMuffinUtil(whereQuery, selectedOptions, limit, sortBy, skip, function(err, muffins) {
    if (err) {
      return handleError(res, err);
    }
    return httpResponse.successResponse(res, muffins);
  });

};


/**
 * Get a single Muffin
 *
 * @param req
 * @param res
 */
exports.show = function(req, res) {
  Muffin.findById(req.params.id, function(err, muffin) {
    if (err) {
      return handleError(res, err);
    }
    if (!muffin) {
      return res.status(404).end();
    }
    return res.status(200).json(muffin);
  });
};

/**
 * Creates a new Muffin in the DB.
 *
 * @param req
 * @param res
 */
exports.create = function(req, res) {

  console.log('req.body', req.body)
  Muffin.create(req.body, function(err, muffin) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(muffin);
  });
};

/**
 * Updates an existing Muffin in the DB.
 *
 * @param req
 * @param res
 */
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Muffin.findById(req.params.id, function(err, muffin) {
    if (err) {
      return handleError(res, err);
    }
    if (!muffin) {
      return res.status(404).end();
    }
    var updated = _.merge(muffin, req.body);
    updated.save(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(muffin);
    });
  });
};

/**
 * Deletes a Muffin from the DB.
 *
 * @param req
 * @param res
 */
exports.destroy = function(req, res) {
  Muffin.findById(req.params.id, function(err, muffin) {
    if (err) {
      return handleError(res, err);
    }
    if (!muffin) {
      return res.status(404).end();
    }
    muffin.remove(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).end();
    });
  });
};


exports.getMyMuffins = function(req, res) {
  MuffinsForUsers.find({}, function(err, muffin) {

  });

};

exports.joinMuffin = function(req, res) {
  var requiredFields = ['muffinId', 'userId'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    Muffin.findOne({
      _id: req.body.muffinId
    }, function(error, muffinResult) {
      if (error) {
        handleError(res, error);
      } else {
        if (!muffinResult || muffinResult === {}) {
          Success.errorResponse(res, 'Something went wrong', 500, 'Invalid Muffin');
        } else {
          var createObj = {};
          createObj.muffinId = req.body.muffinId;
          createObj.userId = req.body.userId;
          createObj.relationType = 'pending';
          createObj.IsMeetingDone = req.user.kycDone && req.user.creditAssessmentDone;
          createObj.MeetingDate = moment().add(2, 'd').hour('5').minute('30').toString();
          MuffinsForUsers.create(createObj, function(error, createResult) {
            if (error) {
              handleError(res, error);
            } else {
              return httpResponse.successResponse(res, createResult);
            }
          });
        }
      }
    });
  }
};


function createAuctionObjects(muffin_id, members, auctionDate) {

  var auctionObj = {
    "muffinId": muffin_id,
    "auctionDate": auctionDate,
  };
  for (var i = 0; i < members; i++) {
    var dateValue = new Date(auctionDate);
    console.log('This is the dates', dateValue, auctionDate, dateValue.getMonth());
    // dateValue.setMonth( dateValue.getMonth() + i );
    dateValue.setMinutes(dateValue.getMinutes() + i * .1);
    if (dateValue.getMonth() + 1 > 12) dateValue.setYear(dateValue.getYear() + 1); // adding changes to save the date
    var newStatusValue = "bidding_active";

    var args = {
      data: {
        time_delay: dateValue,
        new_state: newStatusValue,
        muffinId: muffin_id
      },
      headers: {
        "Content-Type": "application/json"
      }
    };
    //call api to schedule the job @todo have to add JS URL
    client.post("http://localhost:9900/api/schedule/jobs/schedule/job", args, function(data, response) {
      console.log(data);
    });


    var bidOffTime = new Date(dateValue);
    newStatusValue = "active";
    bidOffTime.setMinutes(bidOffTime.getMinutes() + 10);
    args = {
      data: {
        time_delay: bidOffTime,
        new_state: newStatusValue,
        muffinId: muffin_id
      },
      headers: {
        "Content-Type": "application/json"
      }
    };
    //call api to schedule the job @todo have to add JS URL
    client.post("http://localhost:9900/api/schedule/jobs/schedule/job", args, function(data, response) {
      console.log(data);
    });
    auctionObj.auctionDate = dateValue;
    auctionObj.auctionNumber = i;
    Auctions.create(auctionObj, function(err, data) {});
  }
}


exports.findMuffinsByUserId = function(req, res) {

  var whereQuery = {},
    selectedOptions = {},
    limit = 10,
    page = 0,
    sortBy, skip;

  if (req.params.userId) whereQuery['createdBy'] = req.params.userId;
  if (req.params.limit) limit = req.params.limit;
  if (req.params.page) page = req.params.page;


  findMuffinUtil(whereQuery, selectedOptions, limit, sortBy, skip, function(err, muffins) {
    if (err) {
      return handleError(res, err);
    }
    return httpResponse.successResponse(res, muffins);
  });

};


exports.createMuffinByOfficeUsers = function(req, res) {

  var userId = req.user._id;

  req.body.createdBy = userId;
  req.body.regNumber = randtoken.generate(5);

  console.log('------>', req.body);
  Muffin.create(req.body, function(err, muffin) {
    if (err) return httpResponse.errorResponse(res, err, 500);

    console.log('This is --->', muffin);
    var muffinId = muffin['_id'];
    console.log('Current User', req.user);
    var muffinForUserObject = {};
    muffinForUserObject.userId = userId;
    muffinForUserObject.muffinId = muffinId;
    muffinForUserObject.relationType = 'subscribed';
    muffinForUserObject.startsIn = 40; //ßåHardcoded Starts in date
    if (!req.user.kycDone || !req.user.creditAssessmentDone) {
      muffinForUserObject.MeetingDate = moment().add(2, 'd').hour('5').minute('30').toString();
      muffinForUserObject.IsMeetingDone = false;
    } else {
      muffinForUserObject.MeetingDate = moment().add(2, 'd').hour('5').minute('30').toString();
      muffinForUserObject.IsMeetingDone = true;
    }
    console.log('------->', muffinForUserObject);
    MuffinsForUsers.create(muffinForUserObject, function(err, muffincreated) {

      console.log("vvjvajsvdjas11111111111" + muffin);

      GetDeeplinkUrl.getUrl(muffinId, userId, muffin.name, function(url) {
        console.log("vvjvajsvdjas222222222222222" + muffin);

        muffin.link = url;

        muffin.save();

        return httpResponse.successResponse(res, {
          muffin: muffin
        });

      });

    });


  });

};


exports.adminState = function(req, res) {
  var state = Muffin.schema.path('state').enumValues;

  console.log('ye lo bhai', state);
  var muffinId = req.body._id;
  Muffin.findOne(req.body, function(err, data) {
    if (err) {
      console.log('nhi aya');
      return false;
    } else {

      var mydata = [];
      mydata.push('result:', data);
      res.status(200).send(mydata);
    }

  })

}


exports.stateApprove = function(req, res) {
  Muffin.find(function(err, data) {
    if (err) {
      res.status(200).send({
        status: "error",
        message: "Something went wrong, Please try again.",
        errorInfo: err
      })
    }

    var checkData = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i].state != 'rejected_at_approval' && data[i].state != 'active') {
        checkData.push(data[i]);
      }
      if (checkData.craetedByAdminType = 'admin') {
        checkData.status = 'reg_queue_previous_sanction';
      }
    }
    res.status(200).send({
      status: "success",
      response: checkData
    });

  });
};


exports.muffinState = function(req, res) {
  var requiredFields = ['muffinId', 'name'];
  var paramCheck = utils.hasSufficientParams(req.body, requiredFields, [], []);
  if (!paramCheck.success) {
    return Errors.errorMissingParam(res, paramCheck.data.toString());
  } else {
    Muffin.findOne({
      _id: req.body.muffinId
    }, function(error, muffinResult) {

      if (error) {
        handleError(res, error);
      } else {
        if (!muffinResult || muffinResult === {}) {
          Success.errorResponse(res, 'Something went wrong', 500, 'Invalid Muffin');

        } else {
          var createObj = {};
          createObj.name = req.body.name;
          createObj.muffinId = req.body.muffinId;
          createObj.craetedByAdminType = req.body.craetedByAdminType;
          createObj.stateAcceptDecline = req.body.stateAcceptDecline;
          createObj.state = req.body.state;
          var state = createObj.state;

          if (state == 'reg_queue_previous_sanction' || 'approval_queue') {
            if (createObj.craetedByAdminType == 'admin' || 'superadmin') {
              createObj.state = 'reg_queue_previous_sanction';
            }
            if (state == 'reg_queue_previous_sanction') {
              if (createObj.stateAcceptDecline == true) {
                createObj.state = 'subscription_queue';
              }
            }
            if (state == 'subscription_queue') {
              if (createObj.stateAcceptDecline == true) {
                createObj.state = 'reg_queue_registraion';
              }
            }
            if (state == 'reg_queue_registraion') {
              if (createObj.stateAcceptDecline == true) {
                createObj.state = 'reg_queue_commencement';
              }
            }
            if (state == 'reg_queue_commencement') {
              if (createObj.stateAcceptDecline == true) {
                createObj.state = 'reg_queue_completion';
              }
            }
            if (state == 'reg_queue_completion') {
              if (createObj.stateAcceptDecline == true) {
                createObj.state = 'active';
              }
            }
            if (state == 'active') {
              if (createObj.stateAcceptDecline == true) {
                createObj.state = 'bidding_active';
              }
            }
            if (state == '') {
              if (createObj.stateAcceptDecline == false) {
                createObj.state = 'rejected_at_approval';
              }
            }

            Muffin.findByIdAndUpdate(createObj.muffinId, createObj, {
              new: true
            }, function(error, createResult) {

              if (error) {
                res.status(200).send({
                  status: "error",
                  message: "Something went wrong, Please try again.",
                  errorInfo: err
                })
              } else {
                return res.status(200).send({
                  status: "success",
                  response: createResult
                });
              }

            });
          }
        }
      }
    });

  }
};