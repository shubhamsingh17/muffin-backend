'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var authTypes = ['github', 'twitter', 'facebook', 'google'];
//var ShortId = require('mongoose-shortid');

var MuffinSchema = new Schema({

  // _id: {
  //       type: ShortId,
  //       len: 4,  
  //       retries: 4 
  // },
  name: {
    type: String,
    required: true
  },
  code: String,
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  createdByAdmin: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MuffinOfficeUsers'
  },
  craetedByAdminType: {
    type: String
  },
  faceValue: String,
  regNumber: String,
  regDate: String,
  state: {
    type: String,
    enum: [
      'approval_queue',
      'subscription_queue',
      'reg_queue_previous_sanction',
      'reg_queue_registration',
      'reg_queue_commencement',
      'reg_queue_completion',
      'active',
      'release',
      'archived',
      'bidding_active',
      'rejected_at_approval'
    ],
    default: "approval_queue"
  },
  link: String,
  statusMessage: String,
  currentBidding: {
    type: Number,
    default: 0
  },
  biddingTimes: [Date],

  doc: {},

  fd: {

    bankName: String,
    bankCode: String,
    duration: Number,
    rate: Number,
    startDate: {
      type: Date,
      default: Date.now
    },
    endDate: {
      type: Date,
      default: Date.now
    }

  },

  bankGaurantee: {

    bankName: String,
    bankCode: String,
    duration: Number,
    startDate: {
      type: Date,
      default: Date.now
    },
    endDate: {
      type: Date,
      default: Date.now
    },
    charges: Number

  },

  commenceDate: Date,
  firstAuctionDate: Date,
  lastAuctionDate: Date,
  chitValue: Number,
  instalment: Number,
  members: Number,
  frequency: String,
  commission: Number,
  minPrizeAmt: Number,
  startsIn: Number,

  lateFeeCharges: Number,
  auctionTimeSlot: String,

  created_at: {
    type: Date,
    default: Date.now
  },
  defaulters: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],
  stateAcceptDecline :{
    type: Boolean,
    default: false 
  }


});


module.exports = mongoose.model('Muffin', MuffinSchema);