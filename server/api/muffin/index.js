'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./muffin.controller');
var auth = require('../../auth/auth.service');


router.get('/get/muffins/:muffin_id?/:muffin_state?/:limit?/:page?', controller.findMuffins);

router.post('/create', auth.isAuthenticated(), controller.createMufffin); // @todo have to uncomment auth 


 router.get('/allMuffins', controller.index);
// router.get('/:id', controller.show);

router.post('/', controller.create);
router.post('/joinMuffin', auth.isAuthenticated(), controller.joinMuffin);

router.put('/:id', controller.update);
router.put('/activateMuffin', controller.activateMuffin);

router.post('/createMuffinByOfficeUsers', auth.isLegalSalesAuth(),  controller.createMuffinByOfficeUsers);
router.delete('/:id', controller.destroy);
router.get('/findMuffinsByUserId', controller.findMuffinsByUserId);
router.post('/state', controller.muffinState);


module.exports = router;