'use strict';

var Muffin = require('./muffin.model');

exports.register = function (socket) {

  Muffin.schema.post('save', function (doc) {
    socket.emit('Muffin:save', doc);
  });

  Muffin.schema.post('remove', function (doc) {
    socket.emit('Muffin:remove', doc);
  });

};
