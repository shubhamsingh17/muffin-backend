'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./reports-chargeswaiver.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/findbyid', controller.findid);

router.post('/new', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;