'use strict';

var mongoose = require('mongoose');



var Schema = mongoose.Schema;

var ReportsChargeswaiverSchema = new Schema({

  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  muffinId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Muffin'
  },
  auctionId:{
    type: mongoose.Schema.Types.ObjectId,
      ref: 'Auctions'
  },
  amountDue: {
    type: Number
  },
  amountPaid: {
    type: Number
  },
  auctionNum: {
    type: Number
  },
  date: {
    type: Date
  },
  isWaived: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model('ReportsChargeswaiver', ReportsChargeswaiverSchema);