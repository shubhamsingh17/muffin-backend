'use strict';

var config = require('./config/environment');

module.exports = function(app) {

  // API
  app.use('/api/documents', require('./api/documents'));
  app.use('/api/devices', require('./api/devices'));
  app.use('/api/transactions', require('./api/transaction'));
  app.use('/api/statements', require('./api/statements'));
  app.use('/api/contact-us', require('./api/contact-us'));
  app.use('/api/activities', require('./api/activity'));
  app.use('/api/companyDocs', require('./api/company-docs'));
  app.use('/api/auctions', require('./api/auctions'));
  app.use('/api/muffins-for-users', require('./api/muffins-for-users'));
  app.use('/api/muffins', require('./api/muffin'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/dashboard', require('./api/dashboard'));
  app.use('/api/s3store', require('./api/s3store'));
  app.use('/api/banks', require('./api/bank'));
  app.use('/api/legals', require('./api/legal'));
  app.use('/api/muffin-office-users', require('./api/muffin-office-users'));
  app.use('/api/sales', require('./api/sales'));
  app.use('/api/tasks', require('./api/task'));
  app.use('/api/reports-installmentpayable', require('./api/reports-installmentpayable'));
  app.use('/api/reports-latefeecharges', require('./api/reports-latefeecharges'));
  app.use('/api/reports-paymentdeclined', require('./api/reports-paymentdeclined'));
  app.use('/api/reports-delayedpaymentinterest', require('./api/reports-delayedpaymentinterest'));
  app.use('/api/reports-chargeswaiver', require('./api/reports-chargeswaiver'));
  app.use('/api/reports-bouncecharges', require('./api/reports-bouncecharges'));
  app.use('/api/reports-auctionwon', require('./api/reports-auctionwon'));
  app.use('/api/reports-amountpaid', require('./api/reports-amountpaid'));
  app.use('/api/reports-dividendreceived', require('./api/reports-dividendreceived'));
  app.use('/api/report', require('./api/reports'));
  // Auth
  app.use('/auth', require('./auth'));

  app.route('/:url(api|app|bower_components|assets)/*')
    .get(function(req, res) {
      res.status(404).end();
    });

  app.route('/*')
    .get(function(req, res) {
      res.sendFile(
        app.get('appPath') + '/index.html', {
          root: config.root
        }
      );
    });

};