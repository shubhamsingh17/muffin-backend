#!/bin/bash
export NODE_ENV=production

# Invoke the Forever module (to START our Node.js server).
forever \
start \
-al forever.log \
-ao out.log \
-ae err.log \
server/server.js
